# Time vizualiser

An experiment to recreate the current weather conditions with ThreeJS and WebGL

# Article

## The inspiration

At night, when waking up from insomnia, I want to know how much night is left.
The project degenerated when I learned more about the different phases of night, dawn and dusk.
Plus, being a lover of atmospheres, I wanted to spot the golden hour times.

## The workings

There are 3 several layers : 

0 - background gradient - the atmosphere
1-  the star canvas
2 - the rain canvas
3 - the clouds canvas


There are several web services / libraries needed :
- sun position calculator
- geo coding
- weather
- ocean names
- timezone api
The vizualisation may break because of the absence of these services.

## The background gradient

The library sunCalc subdivises the day in phases : each one has its gradient

## The star canvas

## The rain canvas

## The clouds canvas

## The GUI
I used lil-gui for this part

# TODO
* refatoring to make code clearer
* isolate dependencies on public web services
