const openWeatherMapApiKey = 'f482f1ef6eec19671a485d6563ef4c1f';
function getWeatherService(lat, long) {
    return `https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${long}&appid=${openWeatherMapApiKey}`;
}

function getOceanService(lat, long) {
    return `http://api.geonames.org/ocean?lat=${lat}&lng=${long}&username=pyrene`;
}

export default async function getWeatherConfiguration(lat, long) {
    let conf = {};
    const response = await fetch(getWeatherService(lat, long));
    const json = await response.json();

    // === the decision to use the wind value or not is to be made externally
    conf.windDirection = +json.wind.deg;
    conf.windSpeed = +json.wind.speed;
    // update weather text display 
    conf.weatherLabel = "";
    if (json.name) {
        conf.weatherLabel = json.weather[0].main + ' at ' + json.name + ' (' + json.sys.country + ')';
    } else {
        const responseOcean = await fetch(getOceanService(conf));
        const text = await response.text();
        let tokenStart = '<name>';
        let tokenEnd = '</name>';
        let indexStart = text.indexOf(tokenStart) + tokenStart.length;
        let indexEnd = text.indexOf(tokenEnd);
        conf.weatherLabel = json.weather[0].main + ' over the ' + text.substring(indexStart, indexEnd);
    } 

    conf.weatherDescLabel = json.weather[0].description;
    conf.weatherIcon = `http://openweathermap.org/img/wn/${json.weather[0].icon}.png`;
    conf.isClear = json.weather[0].main.toUpperCase() === "CLEAR";

    // atmosphere default value
    // TODO : the saturation of the background gradient should be function of the cloud density. The more clouds, the grayer.
    conf.cloudDensity = 1;
    if (conf.weatherDescLabel === "overcast clouds") {
        conf.cloudDensity = 1200;
    } else if (conf.weatherDescLabel === "broken clouds") {
        conf.cloudDensity = 400;
    } else if (conf.weatherDescLabel === "scattered clouds") {
        conf.cloudDensity = 250;
    } else if (conf.weatherDescLabel === "few clouds") {
        conf.cloudDensity = 150;
    }

    return conf;

    // fetch(getWeatherService(conf)).then((response) => response.json()).then((json) => {
    //     // update values

    //     // === the decision to use the wind value or not is to be made externally
    //     // 
    //     // if (conf.isWindManaged) {
    //     //     // gui.controllers.forEach(v => v.updateDisplay()); -- manage gui externally
    //     //     conf.windDirection = +json.wind.deg;
    //     //     conf.windSpeed = +json.wind.speed;
    //     // }

    //     conf.windDirection = +json.wind.deg;
    //     conf.windSpeed = +json.wind.speed;

    //     // update weather text display 
    //     conf.weatherLabel = "";
    //     if (json.name) {
    //         conf.weatherLabel = json.weather[0].main + ' at ' + json.name + ' (' + json.sys.country + ')';
    //     } else {
    //         //const response = await fetch(getOceanService(conf));

    //         fetch(getOceanService(conf)).then((response) => {
    //             return response.text();
    //         }).then((text) => {
    //             let tokenStart = '<name>';
    //             let tokenEnd = '</name>';
    //             let indexStart = text.indexOf(tokenStart) + tokenStart.length;
    //             let indexEnd = text.indexOf(tokenEnd);
    //             conf.weatherLabel = json.weather[0].main + ' over the ' + text.substring(indexStart, indexEnd);
    //         });
    //     }  
    //     conf.weatherDescLabel = json.weather[0].description;
    //     conf.weatherIcon = `http://openweathermap.org/img/wn/${json.weather[0].icon}.png` ;

    //     // - - - - - Generate weather parameters

    //     // for the snow : https://codepen.io/bsehovac/pen/GPwXxq
    //     // for the rain : https://codepen.io/navin_moorthy/pen/qBWYORJ

    //     // atmosphere default value
    //     // TODO : the saturation of the background gradient should be function of the cloud density. The more clouds, the grayer.
    //     conf.CloudDensity = (json.weather[0].main.toUpperCase() === "CLEAR");
    //     conf.CloudDensity = 1;
    //     if (weatherDescLabel.innerText === "overcast clouds") {
    //         conf.CloudDensity = 1200;
    //     } else if (weatherDescLabel.innerText === "broken clouds") {
    //         conf.CloudDensity = 400;
    //     } else if (weatherDescLabel.innerText === "scattered clouds") {
    //         conf.CloudDensity = 250;
    //     } else if (weatherDescLabel.innerText === "few clouds") {
    //         conf.CloudDensity = 150;
    //     }
        
    // });
    
    // res(conf);
}