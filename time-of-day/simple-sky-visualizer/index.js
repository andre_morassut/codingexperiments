import * as GUI from './lib/lil-gui/lil-gui.umd.js';
import * as CAL from './lib/suncalc-1.9.0/suncalc.js';

import getWeatherConfiguration from './weather.js';

// ------------------------------------------------
// parcel must know the images are referenced - i despise this workaround
import firstquarter from './first-quarter.svg';
import fullmoon from './full-moon.svg';
import lastquarter from './last-quarter.svg';
import newmoon from './new-moon.svg';
import waningcrescent from './waning-crescent.svg';
import waninggibbous from './waning-gibbous.svg';
import waxingcrescent from './waxing-crescent.svg';
import waxinggibbous from './waxing-gibbous.svg';
let moonImages = {
    newmoon,
    waxingcrescent,
    firstquarter,
    waxinggibbous,
    fullmoon,
    waninggibbous,
    lastquarter,
    waningcrescent
}
// ------------------------------------------------
// DOCUMENT ELEMENTS
const weatherLabel = document.getElementById('weatherLabel');
const weatherDescLabel = document.getElementById('weatherDescLabel');
const weatherIcon = document.getElementById('weatherIcon');
const moonPhase = document.getElementById('moonPhase');
const moonImage = document.getElementById('moonImage');
const phase = document.getElementById('dayOrNightPhase');
const timeElt = document.getElementById('time');
// ------------------------------------------------

const gui = new GUI.GUI();
gui.close();
let updateDateAuto = true;
let now;
const conf = {
    lat: 0,
    long: 0,
    resetLocation: resetLocation,
    day: 1,
    month: 0,
    year: 1999,
    hour: 0,
    minute: 0,
    resetDate: resetDate,
    isWindManaged: true,
    windSpeed: 1,
    windDirection: 45,
    openWeatherMapApiKey: 'f482f1ef6eec19671a485d6563ef4c1f',
    isDay: true,
    weatherScene3dAlreadyInitialized: false,
    weatherAnimationFrameId: null,
    starsScene3dAlreadyInitialized: false,
    starsAnimationFrameId: null,
    addOrRemoveAxesHelper: addOrRemoveAxesHelper,
};
gui.add(conf, 'lat', -80, 80).name('Latitude'); // high latitude returs NAN and invalid times in sunCalc JS (https://github.com/mourner/suncalc/issues/45)
gui.add(conf, 'long', -180, 180).name('Longitude');
gui.add(conf, 'isWindManaged').name('Let weather service update wind values?');
gui.add(conf, 'windSpeed', 0, 50).name('Wind speed');
gui.add(conf, 'windDirection', 0, 359).name('Wind direction');
gui.add(conf, 'resetLocation').name('Reset location');
let dateGui = gui.addFolder('Date');
dateGui.add(conf, 'day');
dateGui.add(conf, 'month', {January: 0, February: 1, March: 2, April: 3, May: 4,
                            June: 5, July: 6, August: 7, September: 8, October: 9,
                            November: 10, December: 11});
dateGui.add(conf, 'year', 0, 3000, 1);
dateGui.add(conf, 'hour', 0, 23, 1);
dateGui.add(conf, 'minute', 0, 59, 1);
gui.add(conf, 'resetDate').name('Reset date/time');
let miscGui = gui.addFolder('Misc.');
miscGui.add(conf, 'addOrRemoveAxesHelper').name('Add/remove axes to 3d scene');

// TODO Actualize hour with position using offline timezone calculator (e.g. https://github.com/mapbox/timespace)



initializeLocation();

function updateTime() {
    timeElt.innerText = conf.hour + ':' + (conf.minute < 10 ? '0' + conf.minute : conf.minute);
    // if (updateDateAuto) {
    // 	let offset = now.getTimezoneOffset() / 60;
    // 	timeElt.innerText += ' (GMT' + (offset < 0 ? ' - ' : ' + ') + Math.abs(offset) + ')';
    // } else {

    // }
}


function initializeLocation() {
    navigator.geolocation.getCurrentPosition((position) => {
        conf.lat = position.coords.latitude;
        conf.long = position.coords.longitude;
        gui.controllers.forEach(v => v.updateDisplay());

        resetDate();
        setInterval(resetDate, 2000);

        updateSunMoon();
        updateWeather();
        gui.onChange(() => {
            resetDate().then(() => {
                updateSunMoon();
                updateWeather();
            })
        });
        dateGui.onChange(setManualDate);
        setInterval(updateSunMoon, 60000);
        setInterval(updateWeather, 60000 * 60);
    });
}



function setManualDate() {
    updateDateAuto = false;
    updateSunMoon();
}

function resetLocation() {
    navigator.geolocation.getCurrentPosition((position) => {
        conf.lat = position.coords.latitude;
        conf.long = position.coords.longitude;
        gui.controllers.forEach(v => v.updateDisplay());
        updateSunMoon();
        updateWeather();
    });
}

function resetDate() {
    let zoneUrl = `http://api.geonames.org/timezone?lat=${conf.lat}&lng=${conf.long}&username=pyrene`;
    return fetch(zoneUrl)
    .then((data) => {
        return data.text();
    })
    .then((text) => {
        let tokenStart = '<gmtOffset>';
        let tokenEnd = '</gmtOffset>';
        let indexStart = text.indexOf(tokenStart) + tokenStart.length;
        let indexEnd = text.indexOf(tokenEnd);
        let offset = +text.substring(indexStart, indexEnd);
        let localOffset = +(new Date().getTimezoneOffset() / 60);
        let shortOffsetVal = 'GMT' + (offset > 0 ? '+' + offset : offset);
        now = new Date(Date.now() + ((localOffset + offset) * 60 * 60 * 1000));
        /*
        Date d = new Date();
        d.setTime(d.getTime() + (offset * 60));
        d;
        */
        conf.day = now.getDate();
        conf.month = now.getMonth();
        conf.year = now.getFullYear();
        conf.hour = now.getHours();
        conf.minute = now.getMinutes();
        dateGui.controllers.forEach(v => v.updateDisplay());
        updateDateAuto = true;
        updateTime();
    });
}

function updateSunMoon() {
    if (updateDateAuto) {
        now = Date.now();
    } else {
        now = new Date(conf.year, conf.month, conf.day, conf.hour, conf.minute, 0);
    }
    gui.controllers.forEach(v => v.updateDisplay());

    let sunTimes = CAL.getTimes(now, conf.lat, conf.long, 0);
    let moonTimes = CAL.getMoonTimes(now, conf.lat, conf.long);
    let moonIllumination = CAL.getMoonIllumination(now);
    console.log(`Updated @ ${new Date()} with ${JSON.stringify(conf)}`);
    /* SKY */
    let skyColor = 'rgb(100, 0, 0)';
    let sunText;
    let textColor;
    if (now >= sunTimes.nadir && now <= sunTimes.nightEnd) {
        skyColor = 'linear-gradient(0deg, rgba(4,5,20,1) 0%, rgba(0,0,0,1) 50%, rgba(0,4,23,1) 100%)';
        sunText = 'The Night';
        textColor = 'white';
        conf.isDay = false;
    } else if (now > sunTimes.nightEnd && now <= sunTimes.nauticalDawn) {
        skyColor = 'linear-gradient(0deg, rgba(11,13,36,1) 0%, rgba(0,0,0,1) 50%, rgba(0,9,57,1) 100%)';
        sunText = 'The Astronomical Dawn';
        textColor = 'white';
        conf.isDay = false;
    } else if (now > sunTimes.nauticalDawn && now <= sunTimes.dawn) {
        //skyColor = 'linear-gradient(0deg, rgba(14,16,47,1) 0%, rgba(0,0,0,1) 50%, rgba(0,8,46,1) 100%)';
        skyColor = 'linear-gradient(0deg, rgba(16,18,48,1) 0%, rgba(6,11,41,1) 50%, rgba(0,17,96,1) 100%)';
        sunText = 'The Nautical Dawn';
        textColor = 'white';
        conf.isDay = false;
    } else if (now > sunTimes.dawn && now <= sunTimes.sunrise) {
        skyColor = 'linear-gradient(0deg, rgba(48,16,16,1) 0%, rgba(29,43,124,1) 50%, rgba(0,15,113,1) 100%)';
        sunText = 'The Dawn';
        textColor = 'white';
        conf.isDay = false;
    } else if (now > sunTimes.sunrise && now <= sunTimes.sunriseEnd) {
        skyColor = 'linear-gradient(0deg, rgba(94,28,28,1) 0%, rgba(124,77,29,1) 50%, rgba(0,89,113,1) 100%)';
        sunText = 'The Sunrise';
        textColor = 'black';
        conf.isDay = true;
    } else if (now > sunTimes.sunriseEnd && now <= sunTimes.goldenHourEnd) {
        skyColor = 'linear-gradient(0deg, rgba(255,173,24,1) 0%, rgba(196,176,117,1) 50%, rgba(96,125,116,1) 100%)';
        sunText = 'The Morning Golden Hour';
        textColor = 'black';
        conf.isDay = true;
    } else if (now > sunTimes.goldenHourEnd && now <= sunTimes.solarNoon) {
        skyColor = 'linear-gradient(0deg, rgba(255,251,243,1) 0%, rgba(117,183,196,1) 50%, rgba(28,159,210,1) 100%)';
        sunText = 'The Morning';
        textColor = 'black';
        conf.isDay = true;
    } else if (now > sunTimes.solarNoon && now <= sunTimes.goldenHour) {
        skyColor = 'linear-gradient(0deg, rgba(242,222,181,1) 0%, rgba(117,171,196,1) 50%, rgba(28,146,210,1) 100%)';
        sunText = 'The Afternoon';
        textColor = 'black';
        conf.isDay = true;
    } else if (now > sunTimes.goldenHour && now <= sunTimes.sunsetStart) {
        skyColor = 'linear-gradient(0deg, rgba(252,173,71,1) 0%, rgba(203,180,112,1) 50%, rgba(166,188,255,1) 100%)';
        sunText = 'The Evening Golden Hour';
        textColor = 'black';
        conf.isDay = true;
    } else if (now > sunTimes.sunsetStart && now <= sunTimes.sunset) {
        skyColor = 'linear-gradient(0deg, rgba(252,173,71,1) 0%, rgba(203,180,112,1) 50%, rgba(166,188,255,1) 100%)';
        sunText = 'The Sunset';
        textColor = 'white';
        conf.isDay = false;
    } else if (now > sunTimes.sunset && now <= sunTimes.dusk) {
        skyColor = 'linear-gradient(0deg, rgba(252,71,71,1) 0%, rgba(203,148,112,1) 50%, rgba(100,107,149,1) 100%)';
        sunText = 'The Dusk';
        textColor = 'black';
        conf.isDay = false;
    } else if (now > sunTimes.dusk && now <= sunTimes.nauticalDusk) {
        skyColor = 'linear-gradient(0deg, rgba(190,96,96,1) 0%, rgba(88,111,158,1) 50%, rgba(50,53,74,1) 100%)';
        sunText = 'The Nautical Dusk';
        textColor = 'white';
        conf.isDay = false;
    } else if (now > sunTimes.nauticalDusk && now <= sunTimes.night) { 
        skyColor = 'linear-gradient(0deg, rgba(190,96,96,1) 0%, rgba(88,111,158,1) 50%, rgba(50,53,74,1) 100%)';
        sunText = 'The Astronomical Dusk';
        textColor = 'white';
        conf.isDay = false;
    } else if (now > sunTimes.night /*&& now <= times.night*/) {
        skyColor = 'linear-gradient(0deg, rgba(31,33,37,1) 0%, rgba(34,29,60,1) 50%, rgba(0,2,13,1) 100%)';
        sunText = 'The Night';
        textColor = 'white';
        conf.isDay = false;
    } 
    // Apply color and animation
    document.body.style.background = skyColor;
    document.body.style.backgroundSize = '200% 200%';
    document.body.style.animation = 'gradientAnimation var(30s) ease infinite';
    // Apply text
    phase.innerText = sunText;
    phase.style.color = textColor;
    // apply text color to other text elements
    timeElt.style.color = textColor;
    weatherLabel.style.color = textColor;
    weatherDescLabel.style.color = textColor;
    //---------------------------------------------------------------------------------------
    /* MOON */
    let moonText;
    if ((now >= moonTimes.rise && now <= moonTimes.set) || (moonTimes.alwaysUp === true) || (moonTimes.alwaysDown === false)) {
        if (moonIllumination.phase === 0) {
            moonText = 'New Moon';
        } else if (moonIllumination.phase < 0.25) {
            moonText = 'Waxing Crescent';
        } else if (moonIllumination.phase === 0.25) {
            moonText = 'First Quarter';
        } else if (moonIllumination.phase < 0.5) {
            moonText = 'Waxing Gibbous';
        } else if (moonIllumination.phase === 0.5) {
            moonText = 'Full Moon';
        } else if (moonIllumination.phase < 0.75) {
            moonText = 'Waning Gibbous';
        } else if (moonIllumination.phase === 0.75) {
            moonText = 'Last Quarter';
        } else {
            moonText = 'Waning Crescent';
        }
        moonPhase.innerText = moonText;
        moonPhase.style.color = textColor;
        moonImage.src = moonImages[moonText.toLowerCase().replace(' ', '')];
    } else {
        moonPhase.innerText = '';
        moonImage.src = '';
    }
}

function updateWeather() {
    getWeatherConfiguration(conf.lat, conf.long).then((v) => {
        /*
            cloudDensity : 1
            weatherDescLabel : "clear sky"
            weatherIcon : "http://openweathermap.org/img/wn/01d.png"
            weatherLabel : "Clear at Hérault (FR)"
            windDirection : 323
            windSpeed : 6.27
        */
        if (conf.isWindManaged) {
            conf.windDirection = v.windDirection;
            conf.windSpeed = v.windSpeed;
            gui.controllers.forEach(v => v.updateDisplay());
        }
        weatherLabel.innerText = v.weatherLabel;
        weatherDescLabel.innerText = v.weatherDescLabel;
        weatherIcon.src = v.weatherIcon;
        generateWeather(v.isClear, conf.isDay, v.cloudDensity);
        createStars();
        createRain();
    });
}
// TAKEN FROM https://codepen.io/seanseansean/pen/ReMvxV used in galactic backgrounds https://redstapler.co/cool-nebula-background-effect-three-js/
// alternative : draggable volumetric cloud based on fractal brownian motion and raymarching : https://codesandbox.io/s/um3dvk?file=/src/index.js
import * as THREE from './lib/three.r138.module.js';

// ------------------------------------------------
// parcel must know the images are referenced - i despise this workaround
import cloudVariant1 from './cloud-variant-1.png';
import cloudVariant2 from './cloud-variant-2.png';
import starParticle1 from './sp1.png';
import starParticle2 from './sp2.png';
// ------------------------------------------------
let cam, scene, renderer, clock, smokeMaterial, h, w, smokeParticles = [];
let axesHelper;

function addOrRemoveAxesHelper() {
    if (axesHelper) {
        scene.remove(axesHelper);
        axesHelper = null;
    } else {
        axesHelper = new THREE.AxesHelper( 500 )
        scene.add( axesHelper );
    }
}

function generateWeather(isClear, isDay, cloudDensity) {
    let hWorld, wWorld;

    function animate() {
        let delta = clock.getDelta();
        conf.weatherAnimationFrameId = requestAnimationFrame(animate);
        const windDirectionRadiant = conf.windDirection * Math.PI / 180;
        const windXFactor = Math.cos(conf.windDirection * Math.PI / 180);
        const windYFactor = Math.sin(conf.windDirection * Math.PI / 180);
        smokeParticles.forEach(sp => {
            sp.rotation.z += delta * 0.012; // makes clouds rotate on themselves
            sp.position.x += windXFactor * conf.windSpeed/10;
            sp.position.y += windYFactor * conf.windSpeed/10;
            // Compute cloud size by creating a bounding box and getting its size
            let cubeBoundingBox = new THREE.Box3().setFromObject(sp.particleHelper);
            let boxSize = new THREE.Vector3();
            cubeBoundingBox.getSize(boxSize);
            // Compute frustum plane to know world boundaries for the plane corresponding to the z "altitude" of the clouds
            let frustumPlane = computeFrustumPlaneAtHeight(sp.position.z);
            // depending on the corner of the screen, i check the X or Y boundary differently and I take a frustrum slightly larger 
            // than reality to avoir flicker (e.g. clouds "blinking" because they are large and reappear as soon as they disappear)
            let frustumFactor = 1.2;
            if (conf.windDirection < 90) {
                if (((sp.position.x - boxSize.x/2) > frustumPlane[0]/frustumFactor) || ((sp.position.y - boxSize.y/2) > frustumPlane[1]/frustumFactor)) {
                    sp.position.x = -sp.position.x;
                    sp.position.y = -sp.position.y;
                }
            } else if (conf.windDirection < 180) {
                if (((sp.position.x + boxSize.x/2) < -frustumPlane[0]/frustumFactor) || ((sp.position.y - boxSize.y/2) > frustumPlane[1]/frustumFactor)) {
                    sp.position.x = -sp.position.x;
                    sp.position.y = -sp.position.y;
                }
            } else if (conf.windDirection < 270) {
                if (((sp.position.x - boxSize.x/2) < -frustumPlane[0]/frustumFactor) || ((sp.position.y + boxSize.y/2) < -frustumPlane[1]/frustumFactor)) {
                    sp.position.x = -sp.position.x;
                    sp.position.y = -sp.position.y;
                }
            } else {
                if (((sp.position.x - boxSize.x/2) > frustumPlane[0]/frustumFactor) || ((sp.position.y + boxSize.y/2) < -frustumPlane[1]/frustumFactor)) {
                    sp.position.x = -sp.position.x;
                    sp.position.y = -sp.position.y;
                }
            }
            sp.particleHelper.update();
        });
        renderer.render(scene, cam);
    }

    /**
     * Compute width and height of a given plane at a given distance in the fulcrum
     * EQUATION : frustumHeight = 2.0f * distance * Mathf.Tan(camera.fieldOfView * 0.5f * Mathf.Deg2Rad)
     * See: https://docs.unity3d.com/Manual/FrustumSizeAtDistance.html
     * See: https://stackoverflow.com/a/63526427/15393994
     */
    function computeFrustumPlaneAtHeight(planeHeight) {
        let vFOV = (cam.fov * Math.PI) / 180;
        let hPlane = 2 * Math.tan(vFOV / 2) * Math.abs(1000 - planeHeight);
        let wPlane = hPlane * cam.aspect;
        return  [wPlane, hPlane];
    }
    
    function resize() {
        renderer.setSize(window.innerWidth, window.innerHeight);
    }
    
    function lights(pIsDay) {
        const light = new THREE.DirectionalLight(0xffffff, pIsDay ? 1.5 : 0.5); // 1.5 for light clouds
        clock = new THREE.Clock();
        renderer = new THREE.WebGLRenderer({alpha: true});
        renderer.domElement.id = 'weatherCanvas';
        renderer.setSize(w, h);
        scene = new THREE.Scene();
        light.position.set(-1, 0, 1);
        scene.add(light);
    }
    
    function camera() {
        cam = new THREE.PerspectiveCamera(75, w / h, 1,	10000);
        cam.position.z = 1000;
        scene.add(cam);
    }
    
    function action(pDensity) {
        const loader = new THREE.TextureLoader();
        loader.crossOrigin = '';
        loader.load(
            Math.random() > 0.8 ? cloudVariant2 : cloudVariant1, // randomly choose cloud variant
            function onLoad(texture) {
                const smokeGeo = new THREE.PlaneBufferGeometry(300, 300);
                smokeMaterial = new THREE.MeshLambertMaterial({
                    map: texture,
                    transparent: true
                });

;						// compute max and min world visibility - see https://stackoverflow.com/a/63526427/15393994 & https://docs.unity3d.com/Manual/FrustumSizeAtDistance.html
                const vFOV = (cam.fov * Math.PI) / 180;
                hWorld = 2 * Math.tan(vFOV / 2) * Math.abs(cam.position.z);
                wWorld = hWorld * cam.aspect;

                // Generate the clouds
                // The density is function of the weather
                // for each cloud, we want a random "altitude" that will be between 0 (fartest) & max altitude (which is 1000, see camera function) minus a delta 
                // to avoid the cloud being right in the face.
                // then we use the frustrum plane at the given height to compute a random starting within the viewing plane.
                for (let p = 0, l = pDensity; p < l; p++) { 
                    let particle = new THREE.Mesh(smokeGeo, smokeMaterial);

                    let zP = Math.random() * 1000 * 0.75;
                    let frustrumPlaneParticle = computeFrustumPlaneAtHeight(zP); // to use boundaries relative to cloud elevation
                    let xP = (Math.random() * frustrumPlaneParticle[0]*3) - frustrumPlaneParticle[0]/1.5;
                    let yP = (Math.random() * frustrumPlaneParticle[1]*3) - frustrumPlaneParticle[1]/1.5;
                    particle.position.set(xP, yP, zP);
                    particle.rotation.z = Math.random() * 360;
                    scene.add(particle);
                    // optional - to have a box around the clouds and visualize better the collisions
                    particle.particleHelper = new THREE.BoxHelper(particle, 0xffff00);
                    particle.particleHelper.visible = false; // helper can be disabled 
                    scene.add(particle.particleHelper);
                    // keep an array of particles to update them in the animate function
                    smokeParticles.push(particle);
                }
                animate();
            }
        );
    }
        
    function init(pDensity, pIsDay) {
        h = window.innerHeight;
        w = window.innerWidth;
        lights(pIsDay); //💡
        camera(); // 🎥
        action(pDensity); // 🎬
        document.body.insertBefore(renderer.domElement, document.body.firstChild);
        window.addEventListener('resize', resize);
        conf.weatherScene3dAlreadyInitialized = true;
    }

    if (conf.weatherScene3dAlreadyInitialized) {
        cancelAnimationFrame(conf.weatherAnimationFrameId);
        document.body.removeChild(document.body.firstChild);
        window.removeEventListener('resize', resize);
        console.log("---- THREE - Weather scene destroyed");
        conf.weatherScene3dAlreadyInitialized = false;
        conf.weatherAnimationFrameId = null;
    }
    if (!isClear && updateDateAuto) { // if the date is not current, no weather can be displayed
        init(cloudDensity, isDay);
        console.log("---- THREE - Weather scene created");
    }
}

// Adapted from this pen => https://codepen.io/kuntal-das/pen/VwPdGpE (orginal author : https://medium.com/nerd-for-tech/adding-a-custom-star-field-background-with-three-js-79a1d18fd35d)
function createStars() {
    function getRandomParticelPos(particleCount) {
        const arr = new Float32Array(particleCount * 3);
        for (let i = 0; i < particleCount; i++) {
            arr[i] = (Math.random() - 0.5) * 10;
        }
        return arr;
    }

    function resizeRendererToDisplaySize(renderer) {
        const canvas = renderer.domElement;
        const width = canvas.clientWidth;
        const height = canvas.clientHeight;
        const needResize = canvas.width !== width || canvas.height !== height;
        // resize only when necessary
        if (needResize) {
            //3rd parameter `false` to change the internal canvas size
            renderer.setSize(width, height, false);
        }
        return needResize;
    }

    // mouse
    let mouseX = 0;
    let mouseY = 0;
    document.addEventListener("mousemove", (e) => {
        mouseX = e.clientX;
        mouseY = e.clientY;
    });

    function main() {
        const renderer = new THREE.WebGLRenderer({alpha: true });
        renderer.domElement.id = 'starsCanvas';
        document.body.appendChild(renderer.domElement);

        const scene = new THREE.Scene();

        // light source
        const color = 0xffffff;
        const intensity = 1;
        const light = new THREE.DirectionalLight(color, intensity);
        light.position.set(-1, 2, 4);
        scene.add(light);

        // camera
        const fov = 75,
            aspect = 2,
            near = 1.5,
            far = 5;
        const camera = new THREE.PerspectiveCamera(fov, aspect, near, far);
        camera.position.z = 2;

        // Geometry
        const geometrys = [new THREE.BufferGeometry(), new THREE.BufferGeometry()];

        geometrys[0].setAttribute(
            "position",
            new THREE.BufferAttribute(getRandomParticelPos(350), 3)
        );
        geometrys[1].setAttribute(
            "position",
            new THREE.BufferAttribute(getRandomParticelPos(1500), 3)
        );

        const loader = new THREE.TextureLoader();

        // material
        const materials = [
            new THREE.PointsMaterial({
                size: 0.05,
                map: loader.load(starParticle1),
                transparent: true
                // color: "#ff0000"
            }),
            new THREE.PointsMaterial({
                size: 0.075,
                map: loader.load(starParticle2),
                transparent: true
                // color: "#0000ff"
            })
        ];

        const starsT1 = new THREE.Points(geometrys[0], materials[0]);
        const starsT2 = new THREE.Points(geometrys[1], materials[1]);
        scene.add(starsT1);
        scene.add(starsT2);

        const render = (time) => {
            // time *= 0.001; //in seconds

            if (resizeRendererToDisplaySize(renderer)) {
                const canvas = renderer.domElement;
                // changing the camera aspect to remove the strechy problem
                camera.aspect = canvas.clientWidth / canvas.clientHeight;
                camera.updateProjectionMatrix();
            }

            starsT1.position.x = mouseX * 0.0001;
            starsT1.position.y = mouseY * -0.0001;

            starsT2.position.x = mouseX * 0.0001;
            starsT2.position.y = mouseY * -0.0001;

            // Re-render the scene
            renderer.render(scene, camera);
            // loop
            starsAnimationFrameId = requestAnimationFrame(render);
        };
        starsAnimationFrameId = requestAnimationFrame(render);
    }
    if (conf.starsScene3dAlreadyInitialized) {
        cancelAnimationFrame(conf.starsAnimationFrameId);
        document.body.removeChild(document.body.lastChild);
        conf.starsScene3dAlreadyInitialized = false;
        conf.starsAnimationFrameId = null;
        console.log('---- THREE - Stars scene destroyed');
    }
    if (!conf.isDay) {
        main();
        conf.starsScene3dAlreadyInitialized = true;
        console.log('---- THREE - Stars scene created');
    }
}

// --------------- RAIN EFFECT
// taken from : https://codepen.io/navin_moorthy/pen/qBWYORJ
// adapted using : https://sbcode.net/threejs/geometry-to-buffergeometry/ 
function createRain() {
    // SCENE
    let scene = new THREE.Scene();
    scene.fog = new THREE.FogExp2(0x11111f, 0.002);

    /* //////////////////////////////////////// */

    // CAMERA
    let camera = new THREE.PerspectiveCamera(60,window.innerWidth / window.innerHeight,1,1000);
    camera.position.z = 1;
    camera.rotation.x = 1.16;
    camera.rotation.y = -0.12;
    camera.rotation.z = 0.27;

    /* //////////////////////////////////////// */

    // RENDERER
    let renderer = new THREE.WebGLRenderer({alpha: true});
    renderer.domElement.id = 'rainCanvas';
    // changed
    //renderer.setClearColor(scene.fog.color);
    renderer.setSize(window.innerWidth, window.innerHeight);

    // Append canvas to the body
    document.body.appendChild( renderer.domElement);

    /* //////////////////////////////////////// */

    // Ambient Light
    let ambient = new THREE.AmbientLight(0x555555);
    scene.add(ambient);

    /* //////////////////////////////////////// */

    // Directional Light
    let directionalLight = new THREE.DirectionalLight(0xffeedd);
    directionalLight.position.set(0,0,1);
    scene.add(directionalLight);


    /* //////////////////////////////////////// */

    // Point Light
    let flash = new THREE.PointLight(0x062d89, 30, 500, 1.7);
    flash.position.set(200,300, 100);
    scene.add(flash);

    /* //////////////////////////////////////// */

    // Rain Drop Texture
    let rainCount = 29500;
    let cloudParticles=[];
    let rainGeo = new THREE.BufferGeometry();
    let rainGeoPoints = [];
    for(let i=0; i < rainCount; i++) {
        let rainDrop = new THREE.Vector3(
            Math.random()*400-200,
            Math.random()*500-250,
            Math.random()*400-200
        )
        rainDrop.velocity = {};
        rainDrop.velocity = 0;

        rainGeoPoints.push(rainDrop);
    }
    rainGeo.setFromPoints(rainGeoPoints);

    let rainMaterial = new THREE.PointsMaterial({
        color: 0xaaaaaa,
        size: 0.1,
        transparent: true
    })

    let rain = new THREE.Points(rainGeo, rainMaterial);
    scene.add(rain)

    /* //////////////////////////////////////// */

    // Smoke Texture Loader
    // let loader = new THREE.TextureLoader();
    // loader.load("https://raw.githubusercontent.com/navin-navi/codepen-assets/master/images/smoke.png", function(texture) {
    // 	let cloudGeo = new THREE.PlaneBufferGeometry(500,500);
    // 	let cloudMaterial = new THREE.MeshLambertMaterial({
    // 		map:texture,
    // 		transparent: true
    // 	});

    // 	for(let p=0; p<25; p++) {
    // 		let cloud = new THREE.Mesh(cloudGeo, cloudMaterial);
    // 		cloud.position.set(
    // 		Math.random()*800 -400,
    // 		500,
    // 		Math.random()*500-500
    // 		);
    // 		cloud.rotation.x = 1.16;
    // 		cloud.rotation.y = -0.12;
    // 		cloud.rotation.z = Math.random()*2*Math.PI;
    // 		cloud.material.opacity = 0.55;
    // 		cloudParticles.push(cloud);
    // 		scene.add(cloud);
    // 	}
    // });

    /* //////////////////////////////////////// */

    // Render animation on every rendering phase
    function render() {
        renderer.render(scene, camera);
        requestAnimationFrame(render);
        
        // // Cloud Rotation Animation
        // cloudParticles.forEach(p => {
        // 	p.rotation.z -= 0.002;
        // })
        
        // RainDrop Animation
        let positions = rainGeo.attributes.position.array;
        for (let i = 0; i < positions.length; i += 3) {
            let vertex = new THREE.Vector3(positions[i], positions[i + 1], positions[i + 2])
            vertex.velocity = -3*Math.random();
            vertex.y += vertex.velocity;
            if(vertex.y < -100){
                vertex.y = 100;
                vertex.velocity = 0;
            }
            positions[i + 1] = vertex.y;
        }
        rainGeo.attributes.position.needsUpdate = true;
        rain.rotation.y += 0.002;
        
        // Lightening Animation
        if(Math.random() > 0.96 || flash.power > 100) {
            if(flash.power<100) {
            flash.position.set(
                Math.random()*400,
                300+Math.random()*200,
                100
            );
            }
            flash.power = 50 + Math.random() * 500;
        }
    }

    render();

    /* //////////////////////////////////////// */

    // Update Camera Aspect Ratio and Renderer ScreenSize on Window resize
    window.addEventListener( 'resize', function () {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize( window.innerWidth, window.innerHeight );
    }, false );
}