---
title: 'Simple word statistics with NodeJS'
date: '2022-11-28'
tags: nodejs, word statistics, backend
repo: https://gitlab.com/andre_morassut/codingexperiments/-/tree/main/nodejs/word-statistics
---

# Why

This is about learning a language, not a computer language, but a real *human* language. 

The language you speak influences the way you see the world [(1)](#see). Since being a child, I remember feeling that I was not quite the same person, depending on the language I used the most.

After my son was born, I observed that infant learning is instinctual. Could it be that relying on instincts has a positive effect on learning ? I decided to test this hypothesis by learning a new language using a lot of "simple exposition", meaning just hearing people talk. 

The brain is a well-suited tool to sort and memorize repetitions : from time to times, I would use a dictionnary to look up to words I heard often.
Same method with reading, but I lacked a tool to tell me the most frequent words from a written text, so I decided to write one.

# How



# See

> "Two Languages, Two Minds: Flexible Cognitive Processing Driven by Language of Operation" _by Panos Athanasopoulos & al._
>> [Academia.edu : ](https://www.academia.edu/17144577/Two_Languages_Two_Minds_Flexible_Cognitive_Processing_Driven_by_Language_of_Operation) ; [Pub Med (NIH National Library of Medicine) : ](https://pubmed.ncbi.nlm.nih.gov/25749698/) ; [Archive 1 : ](https://archive.ph/2hVJd) ; [Archive 2 : ](http://web.archive.org/web/20220715210421/https://pubmed.ncbi.nlm.nih.gov/25749698/)


