## MAZE 3D -

Starting from the 3D Snake game and using the maze generation algorithm found @ https://www.the-art-of-web.com/javascript/maze-generator/ 

My goal is to make a little maze game for my 5 years old son.

Design goals :
- self explanatory game play
- progressive difficulty
- impersonal player avatar (to avoid rejection)
- no moving enemies
- labyrinth exit should be opened first with a key


# notes
- the inclusion of the score in the threejs rendering loop forces to maintain it alive even between levels, it would have been convenient to shut off the rendering loop between levels but it forces the score and level display to be done at dom level.
With something like : 
```js
export function endRenderingLoop() {
    window.cancelAnimationFrame(requestAnimationFrameId);
    requestAnimationFrameId = undefined;
}
```
Maybe this can serve as an illustration as to why separation of concerns has always some sort of limitation when we deal with parts of a very specialized system.

# Improvements to do

- EN / FR switch
- Help on game controls
- Better grahics
- Better animations