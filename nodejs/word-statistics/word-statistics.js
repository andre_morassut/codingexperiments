const fs = require("fs");

let file = process.argv[2] ?? "./test-text-ru";
let lang = process.argv[3] ?? "en";

fs.readFile(file, "utf8", (err, data) => {
  if (err) {
    console.error(err);
    return;
  }
  const words = data.replace(/\n|\r|\t|,|\*|!|\?|:|\./gm, " ").toLowerCase().split(" ").filter(v => v !== "");
  const longest = words.reduce((ac, v) => Math.max(ac, v.length), 0);
  const shortest = words.reduce((ac, v) =>  v.length > 0 ? Math.min(ac, v.length) : ac, 1);

  let stats = words.reduce((acc, cur, i, arr) => {
      if (acc.has(cur)) {
          acc.set(cur, acc.get(cur) + 1);
      } else {
          acc.set(cur, 1);
      }
      return acc;
  }, new Map());

  // sort map by decreasing occurrence count (see https://stackoverflow.com/a/31159284)
  stats = new Map([...stats.entries()].sort((a, b) => b[1] - a[1]));

  const wordcount = stats.size;
  const separatorSize = 50;
  let content;
  if (lang === "fr") {
    content = ["OCCURRENCES ET FRÉQUENCES\n", 
                      "-".repeat(separatorSize) + "\n",
                      "Taille du mot le plus court : " + shortest + "\n",
                      "Taille du mot le plus long  : " + longest + "\n",
                      "-".repeat(separatorSize) + "\n",
                      "Nombre de mots total du texte initial : " + words.length + "\n",
                      "Nombre de mots différents             : " + wordcount + "\n",
                      "-".repeat(separatorSize) + "\n",
                  ];
  } else {
    content = ["Occurrences & Frequencies\n", 
                      "-".repeat(separatorSize) + "\n",
                      "Shorter word size : " + shortest + "\n",
                      "Longer word size  : " + longest + "\n",
                      "-".repeat(separatorSize) + "\n",
                      "Input word count  : " + words.length + "\n",
                      "Unique word count : " + wordcount + "\n",
                      "-".repeat(separatorSize) + "\n",
                  ];
  }
  for (let [key, val] of stats) {
    content.push(`[${key.padEnd(longest, " ")}] : ${((val / wordcount) * 100).toFixed(2)}% (${val} occurrences)\n`);
  }
  let footer;
  if (lang === "fr") {
    footer = "--- Fin de la liste";
  } else {
    footer = "--- End of list"
  }
  content.push(footer)

  let fileName = "./result@" + Date.now() + ".txt";
  console.log("Writing results to " + fileName);

  fs.writeFile(fileName, content.join(""), err => {
    if (err) {
      console.error(err);
    }
    // file written successfully
    console.log("Resuts written.");
  });
});