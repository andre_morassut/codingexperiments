# Word statistics

A simple NodeJS program to output word frequences, computed from a TXT input file.

## Prerequisites

* NodeJS
* UTF8 text file(s) to analyze

## Usage

Contents
* `word-statistics.js` : the node script
* `result.txt` : a sample result file
* `test*.txt` : various test files, with different character sets

Installation
```
npm install (no dependencies for the moment)
```

Execution
```
node word-statistics [path-to-utf8-txt-file]
```

Output
```
A result.txt file in the node script folder.
```

## Miscellaneous ideas

* support east asian character sets
* stream for large files
* unclutter tags (html, xml) to allow other formats
