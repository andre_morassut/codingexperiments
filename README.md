# Experiments

A place to setup and experiment technologies.

## Three JS

Series about learning and building 3D scenes with ThreeJS
* 001 Hello cube
* 002 Hello better cube
* 003 Lit Cube
* 004 Many cubes
* 005 Responsive cubes
* 006 Primitives (non exhaustive)
* 007 Sun Earth Moon (scenegraph)
* 008 Tank (work in progress)

## NodeJS
* Word statistics : parser displaying word occurrences from a TXT input file

## Snake
* Snake game with ThreeJS

## Time of day
* Recreate current atmosphere with ThreeJS and Web services

## Next.js blog
* Use next.js to make a static website with articles and demos.

## MERN ecommerce website with admin dashboard
* based on a YT tutorial, a collection of three projects : ecommerce front end (react), back end (node, express) and admin dashboard (react)