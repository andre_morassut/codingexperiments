import styled from "styled-components";

const Container = styled.div`
    height: 30px;
    background-color: teal;
    color: white;
    display: flex;
    justify-content: center;
    align-items: center;
    font-size: 14px;
    font-weight: bold;
`;

export default function Announcement() {
  return (
    <Container>New sale season - Winter 2022</Container>
  )
}
