const router = require("express").Router();
const User = require("../models/User");
const CryptoJS = require("crypto-js");
const jwt = require("jsonwebtoken");

function encrypt(pwd) {
    return CryptoJS.AES.encrypt(pwd, process.env.PWD_SECRET).toString();
}

// REGISTER
router.post("/register", async (req, res) => {
    const newUser = new User({
        username: req.body.username,
        email: req.body.email,
        password: encrypt(req.body.password),
    });
    try {
        const savedUser = await newUser.save();
        res.status(201).json(savedUser);
    } catch (err) {
        res.status(500).json(err);
        console.error(err);
    }
});

// LOGIN
router.post("/login", async (req, res) => {
    try {
        const user = await User.findOne({username: req.body.username});
        if (!user) {
            res.status(401).json("Unable to authenticate.");
        } else {
            const hashedPwd = CryptoJS.AES.decrypt(user.password, process.env.PWD_SECRET);
            const storedPassword = hashedPwd.toString(CryptoJS.enc.Utf8);
            if (storedPassword !== req.body.password) {
                res.status(401).json("Unable to authenticate.");
            } else {
                const accessToken = jwt.sign({
                    id: user._id,
                    isAdmin: user.isAdmin
                }, process.env.JWT_SECRET, {expiresIn: "1d"});
                const {password, ...otherData} = user._doc; // destructure to separate object from property
                res.status(200).json({accessToken, ...otherData});
            }
        }
    } catch (err) {
        res.status(500).json(err);
    }
});

module.exports = router;