const { verifyTokenAndAuth, verifyTokenAndAdmin, verifyToken } = require("./verifyToken");
const Order = require("../models/Order");
const router = require("express").Router();

// CREATE ORDER
router.post("/", verifyToken, async (req, res) => {
    const newInstance = new Order(req.body);
    try {
        const savedInstance = await newInstance.save();
        res.status(200).json(savedInstance); 
    } catch (err) {
        res.status(500).json(err);
    }
});

// UPDATE ORDER
router.put("/:id", verifyTokenAndAdmin, async (req, res) => {
    try {
        const updated = await Order.findByIdAndUpdate(req.params.id, {
            $set: req.body
        }, {new: true});
        res.status(200).json(updated);
    } catch (err) {
        res.status(500).json(err);
    }
}); 

// DELETE ORDER
router.delete("/:id", verifyTokenAndAdmin, async (req, res) => {
    try {
        await Order.findByIdAndDelete(req.params.id);
        res.status(200).json("Order deleted.")
    } catch (err) {
        res.status(500).json(err);
    }
})

// GET USER ORDERS
router.get("/find/:userId", verifyTokenAndAuth, async (req, res) => {
    try {
        const orders = await Order.find({userId: req.params.userId});
        res.status(200).json(orders); 
    } catch (err) {
        res.status(500).json(err);
    }
})

// GET ALL ORDERS
router.get("/", verifyTokenAndAdmin, async (req, res) => {
    const query = req.query.new;
    try {
        const orders = query ? await Order.find().sort({ _id: -1 }).limit(5) : await Order.find();
        res.status(200).json(orders);
    } catch (err) {
        res.status(500).json(err); 
    }
});

// GET MONTHLY INCOME - Sample advanced mongo function
router.get("/income", verifyTokenAndAdmin, async (req, res) => {
    const date = new Date();
    const lastMonth = new Date(date.setMonth(date.getMonth() - 1));
    const prevMonth = new Date(date.setMonth(date.getMonth() - 1));

    try {
        const income = await Order.aggregate([
            { $match: { createdAt: { $gte: prevMonth } } },
            { $project: {
                    month: { $month: "$createdAt"},
                    sales: "$amount",
                } 
            },
            { $group: { 
                    _id: "$month",
                    total: { $sum: "$sales" },
                } 
            },
            { $sort : { 
                    _id: 1 
                }
            },
        ]);
        res.status(200).json(income);
    } catch (err) {
        res.status(500).json(err);
    }
});

module.exports = router;