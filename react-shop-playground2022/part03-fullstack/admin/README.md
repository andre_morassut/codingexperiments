ADMIN PART

# Ce que j'ai appris

- **useMemo()** : provides the ability to cache a value between components renders in React
- **css import** : instead of using styled components (cf. client part), Safak uses now a structure where a css file is created for each jsx react component file and imports it by name directly in the component
- **quick and dirty user infos** : the redux state is accessible using localStorage. See in the route management how we access it.

# Bonnes pratiques & points à savoir

useRef => uncontrolled form ; useState
useState => schedules a render every time
useMemo => can be used to skip renders


# Problèmes à résoudre

## Affichage du chart

Dans l'affichage des statistiques utilisateur (Home + Chart), si l'on a des données du type : 

[
    {Nov, x},
    {Dec, y},
    {Jan, z},
]

Où le mois de janvier est celui suivant décembre, alors Chart positionne Janvier en premier.
=> on dirait que c'est aléatoire en plus.

## Affichage du revenu
Je ne sais pas pourquoi le composant FeaturedInfo renvoie toujours un premier useEffect en tableau vide, ce qui m'a obligé à mettre une condition :
```<span className="featuredMoney">{income.length > 0 ? income[1].total : 0}€</span>```jsx
sur la présence des données dans le template. Alors que sur la vidéo (à 2h19m10s) ça ne le fait pas.


# Bon article

### URL : [label](https://asimzaidi.medium.com/advanced-react-optimization-techniques-for-senior-engineers-dafd2cac7883)

Advanced React Optimization Techniques for Senior Engineers
Imagine you are a developer building a trading app dealing with massive amounts of data, it is essential to ensure that the app is fast and performant, especially when rendering heavy UI graphs to the user. There are a number of techniques that you can utilize to enhance the performance of your React app and ensure that it is able to handle the demands of your users. By using event emitters, memoization, virtualization, IndexedDB, streaming data, lazy loading, React Suspense, and concurrent rendering, you can significantly improve the performance and speed of your app and deliver an exceptional user experience.

Event Emitters
One effective way to improve the performance of your React app is to use event emitters. Event emitters allow you to send data from one component to another without having to pass the data through the component hierarchy. This can be especially useful if you have a deep component hierarchy and you want to avoid having to pass data through multiple levels.

2. Memoization

Memoization is a technique that allows you to cache the results of a function so that you don’t have to recalculate them every time the function is called. This can be particularly useful for optimizing expensive calculations that don’t change very often, such as rendering heavy UI graphs.

3. Virtualization

Virtualization is a technique that allows you to only render the components that are visible on the screen, rather than rendering the entire component hierarchy. This can be especially useful for optimizing the performance of large lists or grids, as it reduces the number of components that need to be rendered and allows the app to respond more quickly to user interactions.

4. IndexedDB

IndexedDB is a browser-based database that allows you to store large amounts of data locally in the user’s browser. By using IndexedDB, you can store data locally and retrieve it quickly, rather than having to make a roundtrip to the server every time you need to access it. This can be especially useful for optimizing the performance of apps that deal with a lot of data, such as trading apps.

5. Stream data

Another way to optimize the performance of your React app is to stream data rather than loading it all at once. By streaming data, you can reduce the amount of data that needs to be loaded at once and allow the app to respond more quickly to user interactions.

5. Lazy loading

Lazy loading is a technique that allows you to only load the components that are needed at a given time, rather than loading the entire component hierarchy upfront. This can be especially useful for optimizing the performance of large apps with many components, as it reduces the amount of data that needs to be loaded at once and allows the app to respond more quickly to user interactions.

6. React Suspense

React Suspense is a new feature in React that allows you to declaratively specify how your app should behave while data is being loaded. This can be especially useful for optimizing the performance of apps that deal with a lot of data, as it allows you to specify how the app should behave while data is being loaded, rather than having to manage this manually.

7. Concurrent rendering

Concurrent rendering is a technique that allows your React app to perform multiple rendering tasks at the same time, rather than waiting for one task to complete before starting another. This can be especially useful for optimizing the performance of apps that deal with a lot of data, as it allows the app to respond more quickly to user interactions and deliver a smooth, seamless experience.

By utilizing these techniques, you can enhance the performance and speed of your React app and ensure that it is able to handle the demands of your users. If you are interested in learning more about these techniques or pursuing a career in tech, be sure to visit techmade.co, the world’s first job accelerator.

But the journey doesn’t end here. As a developer, it is always important to keep learning and staying up-to-date with the latest techniques and technologies. If you are passionate about building high-performing and innovative apps, and want to make a meaningful impact in the tech industry, consider pursuing a career in tech. And if you are looking for resources and guidance to help you get started, be sure to visit techmade.co, the world’s first job accelerator. With a focus on providing personalized support and guidance to help you land your dream tech job, https://techmade.co is the perfect partner to help you take your tech career to the next level.

If you have any questions or comments about this article, feel free to leave them in the comments below or reach out to me on [social media platform of your choice]. I am always happy to help and answer any questions you may have.

~ Asim Zaidi

Founder: Techmade (asim@techmade.co)

Senior Software Engineer 
Atlassian

Twitter: _asimzaidi_



----------------------------------

# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: [https://facebook.github.io/create-react-app/docs/code-splitting](https://facebook.github.io/create-react-app/docs/code-splitting)

### Analyzing the Bundle Size

This section has moved here: [https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)

### Making a Progressive Web App

This section has moved here: [https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)

### Advanced Configuration

This section has moved here: [https://facebook.github.io/create-react-app/docs/advanced-configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)

### Deployment

This section has moved here: [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

### `yarn build` fails to minify

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)
