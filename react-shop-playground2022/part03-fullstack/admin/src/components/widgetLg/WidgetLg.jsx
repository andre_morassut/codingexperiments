import { useEffect, useState } from "react";
import { format } from "timeago.js";
import { userRequest } from "../../requestMethods";
import "./widgetLg.css";

export default function WidgetLg() {

  const [orders, setOrders] = useState([]);

  useEffect(() => {
    const getOrders = async () => {
      try {
        const res = await userRequest.get("order/?new=true");
        setOrders(res.data);
      } catch (e) {console.error(e)}
    };
    getOrders();
  }, []);


  const Button = ({ type }) => {
    return <button className={"widgetLgButton " + type}>{type}</button>;
  };
  return (
    <div className="widgetLg">
      <h3 className="widgetLgTitle">Latest transactions</h3>
      <table className="widgetLgTable">
        <tbody>
          <tr className="widgetLgTr">
            <th className="widgetLgTh" scope="col">Customer</th>
            <th className="widgetLgTh" scope="col">Date</th>
            <th className="widgetLgTh" scope="col">Amount</th>
            <th className="widgetLgTh" scope="col">Status</th>
          </tr>
          {orders.map(order => (
            <tr className="widgetLgTr" key={order._id}>
              <td className="widgetLgUser">
                <span className="widgetLgName">{order.userId}</span>
              </td>
              <td className="widgetLgDate">{format(order.createdAt)}</td>
              <td className="widgetLgAmount">{order.amount}</td>
              <td className="widgetLgStatus">
                <Button type={order.status} />
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}
