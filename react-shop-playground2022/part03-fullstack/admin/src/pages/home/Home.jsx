import { useEffect, useMemo, useState } from "react";
import Chart from "../../components/chart/Chart";
import FeaturedInfo from "../../components/featuredInfo/FeaturedInfo";
import WidgetLg from "../../components/widgetLg/WidgetLg";
import WidgetSm from "../../components/widgetSm/WidgetSm";
import { userRequest } from "../../requestMethods";
import "./home.css";

export default function Home() {

  const dataKey = "Active User";

  const [userStats, setUserStats] = useState([]);

  const MONTH = useMemo(() => ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"], []);

  useEffect(() => {
    async function getStats() {
      try {
        const res = await userRequest.get("user/stats");
        res.data.map(v => 
          setUserStats(prev => [
            ...prev,
            {
              name: MONTH[v._id - 1],
              [dataKey]: v.total,
            }
          ])
        )
      } catch (e) {console.error(e)}
    }
    getStats();
  }, [MONTH])
  
  return (
    <div className="home">
      <FeaturedInfo />
      <Chart data={userStats} title="User Analytics" grid dataKey={dataKey}/>
      <div className="homeWidgets">
        <WidgetSm />
        <WidgetLg />
      </div>
    </div>
  );
}
