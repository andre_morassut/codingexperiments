import axios from "axios";

const BASE_URL = "http://localhost:5000/api/";
const TOKEN = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYzYTczNTRjYWMzMjAwYTliZmMzN2I0MSIsImlzQWRtaW4iOnRydWUsImlhdCI6MTY3MjYwNDU1MCwiZXhwIjoxNjcyNjkwOTUwfQ.3UpsOAzU2PrXnCTZF0nHCE47-AN8Q77Ge3aQg5A49G8";

export const publicRequest = axios.create({
    baseURL: BASE_URL,
});

export const userRequest = axios.create({
    baseURL: BASE_URL,
    header: {
        token: "Bearer " + TOKEN
    }
});

export const userRequestWithToken = (token) => {
    return axios.create({
       baseURL: BASE_URL,
       headers: {
           token: "Bearer " + token,
       }
   });
};
