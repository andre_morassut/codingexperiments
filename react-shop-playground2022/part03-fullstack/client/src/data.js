export const sliderItems = [
    {
        id:0,
        img: "https://upload.wikimedia.org/wikipedia/commons/thumb/1/13/Kelly_Bag.jpg/557px-Kelly_Bag.jpg",
        title: "SUMMER SALE",
        desc: "DON'T COMPROMISE ON STYLE! GET FLAT 10% OFF FOR NEW ARRIVALS.",
        bg:"#f5fafd",
    },
    {
        id:1,
        img: "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/Scarf_buckles.JPG/758px-Scarf_buckles.JPG",
        title: "WINTER SALE",
        desc: "DON'T COMPROMISE ON STYLE! GET FLAT 20% OFF FOR NEW ARRIVALS.",
        bg:"#fcf1ed",
    },
    {
        id:2,
        img: "https://upload.wikimedia.org/wikipedia/commons/1/1e/Woman_in_leather_jacket_on_Vespa%2C_Antwerp_Belgium.jpg",
        title: "AUTUMN SALE",
        desc: "DON'T COMPROMISE ON STYLE! GET FLAT 30% OFF FOR NEW ARRIVALS.",
        bg:"#f0fbf4",
    },
]

export const categories = [
    {
        id:0,
        img:"https://upload.wikimedia.org/wikipedia/commons/8/8c/Cream_full_skirt.jpg",
        title:"Women",
        cat:"women",
    },
    {
        id:1,
        img:"https://upload.wikimedia.org/wikipedia/commons/9/93/Leather_jacket.jpg",
        title:"Coats",
        cat:"coat",
    },
    {
        id:2,
        img:"https://upload.wikimedia.org/wikipedia/commons/thumb/1/17/Fall_Fashion_%28Unsplash%29.jpg/1280px-Fall_Fashion_%28Unsplash%29.jpg",
        title:"Jeans",
        cat:"jeans",
    },
];

export const popularProducts = [
    {
        id:0,
        img:"https://upload.wikimedia.org/wikipedia/commons/thumb/9/94/Gold_Short_Jacket_by_Sybil_Connolly.jpg/681px-Gold_Short_Jacket_by_Sybil_Connolly.jpg"
    },
    {
        id:1,
        img:"https://upload.wikimedia.org/wikipedia/commons/1/10/2008_Junya_Watanabe_for_Comme_des_Gar%C3%A7ons_printed_cotton_jacket%2C_RISD_01.jpg"
    },
    {
        id:2,
        img:"https://upload.wikimedia.org/wikipedia/commons/thumb/d/d2/Rabbit_fur_skin_jacket%2C_twistet_2.jpg/416px-Rabbit_fur_skin_jacket%2C_twistet_2.jpg"
    },
    {
        id:3,
        img:"https://upload.wikimedia.org/wikipedia/commons/thumb/0/06/Dress_with_circle_skirt_and_bolero_jacket_-_neutral_standing_pose.jpg/682px-Dress_with_circle_skirt_and_bolero_jacket_-_neutral_standing_pose.jpg"
    },
    {
        id:4,
        img:"https://upload.wikimedia.org/wikipedia/commons/2/23/Striped_skirt_jimmy_choos_3.jpg"
    },
    {
        id:5,
        img:"https://upload.wikimedia.org/wikipedia/commons/thumb/8/87/A_girl_wearing_white_shoes_and_skirt%3B_April_2016_%2803%29.jpg/698px-A_girl_wearing_white_shoes_and_skirt%3B_April_2016_%2803%29.jpg"
    },
    {
        id:6,
        img:"https://upload.wikimedia.org/wikipedia/commons/b/b6/Walter_van_Beirendonck_dress.jpg"
    },
    {
        id:7,
        img:"https://upload.wikimedia.org/wikipedia/commons/thumb/d/d8/Hand_knitted_socks.jpg/1169px-Hand_knitted_socks.jpg"
    },
    {
        id:8,
        img:"https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Traditional_countryside_slippers_in_Zhejiang.jpg/1024px-Traditional_countryside_slippers_in_Zhejiang.jpg"
    },
];