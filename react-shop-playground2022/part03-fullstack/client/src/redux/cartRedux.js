import { createSlice } from "@reduxjs/toolkit";

const cartSlice = createSlice({
    name: "cart",
    initialState: {
        products: [],
        quantity: 0,
        total:0,
    },
    reducers: {
        addProduct: (state, action) => {
            state.quantity += action.payload.quantity;
            state.products.push(action.payload.product);
            state.total = Math.round((state.total + action.payload.price) * 100) / 100;
        }
    }
});

export const {addProduct} = cartSlice.actions;
export default cartSlice.reducer;