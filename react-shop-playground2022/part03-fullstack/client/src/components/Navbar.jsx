import Badge from '@material-ui/core/Badge';
import Search from "@material-ui/icons/Search";
import ShoppingCartOutlined from '@material-ui/icons/ShoppingCartOutlined';
import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { Link } from "react-router-dom";
import styled from "styled-components";
import Announcement from '../components/Announcement';
import { mobile } from '../responsive';

const Container = styled.div`
    height: 90px;

    transition: transform 0.3s ease-in, background-color 0.3s linear;
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    background-color: white;
    z-index: 900;

    transform: translateY(${props => props.scrollup === "down" ? "-100%" : 0});

    ${mobile({ height: "50px" })}
`;
const Wrapper = styled.div`
    padding: 10px 20px;
    display: flex;
    align-items: center;
    justify-content: space-between;
    ${mobile({ padding: "10px 0px" })}
`;
const Language = styled.span`
    font-size: 14px;
    cursor: pointer;
    ${mobile({ display: "none" })}
`;
const SearchContainer = styled.div`
    border: 0.5px solid lightgray;
    display: flex;
    align-items: center;
    margin-left: 25px;
    padding: 5px;
`;
const Input = styled.input`
    border: none;
    ${mobile({ width: "50px" })}
`;
const Left = styled.div`
    flex: 1;
    display: flex;
    align-items: center;
`;
const Center = styled.div`
    flex: 1;
`;
const Logo = styled.h1`
    font-weight: bolder;
    text-align: center;
    ${mobile({ fontSize: "24px" })}
`;
const TitleLink = styled(Link)`
    text-decoration: none;
`;
const Right = styled.div`
    flex: 1;
    display: flex;
    align-items: center;
    justify-content: flex-end;
    ${mobile({ flex:2, justifyContent: "center" })}
`;
const MenuItem = styled.div`
    font-size: 14px;
    cursor: pointer;
    margin-left: 25px;
    ${mobile({ fontSize: "12px", marginLeft: "10px" })}
`;
const MenuLink = styled(Link)`
    text-decoration: none;
    color: black;
`;

export default function Navbar() {
    const quantity = useSelector(state => state.cart.quantity);
    const useScrollDirection = () => {
        const [scrollDirection, setScrollDirection] = useState('')
        const [prevOffset, setPrevOffset] = useState(0)
    
        const toggleScrollDirection = () => {
          let scrollY = window.scrollY
          if (scrollY > prevOffset && scrollY > 90) {
            setScrollDirection('down')
          } else if (scrollY < prevOffset) {
            setScrollDirection('up')
          } else {
            setScrollDirection('')
          }
          setPrevOffset(scrollY)
        }

        useEffect(() => {
            window.addEventListener('scroll', toggleScrollDirection)
            return () => {
              window.removeEventListener('scroll', toggleScrollDirection)
            }
          })
          return scrollDirection
        }
    const scrollDirection = useScrollDirection()
  return (
    <Container scrollup={scrollDirection}>
        <Announcement />
        <Wrapper>
            <Left>
                <Language>EN</Language>
                <SearchContainer>
                    <Input placeholder="Search" />
                    <Search style={{color:"gray", fontSize:16}} />
                </SearchContainer>
            </Left>
            <Center>
                <TitleLink to="/">
                    <Logo>RECURSIVE.</Logo>
                </TitleLink>
            </Center>
            <Right>
                <MenuLink to="/register">
                    <MenuItem>REGISTER</MenuItem>
                </MenuLink>
                <MenuLink to="/login">
                    <MenuItem>SIGN IN</MenuItem>
                </MenuLink>
                <MenuLink to="/cart">
                    <MenuItem>
                        <Badge badgeContent={quantity} color="primary">
                            <ShoppingCartOutlined color="action" />
                        </Badge>
                    </MenuItem>
                </MenuLink>
            </Right>
        </Wrapper>
    </Container>
  )
}
