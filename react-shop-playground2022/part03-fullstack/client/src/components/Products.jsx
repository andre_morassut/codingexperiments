import axios from "axios";
import { useEffect, useState } from "react";
import styled from "styled-components";
import Product from "./Product";

const Container = styled.div`
    padding: 20px;
    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;
`;

export default function Products({cat, filters, sort}) {

  const [products, setProducts] = useState([]);
  const [filteredProducts, setFilteredProducts] = useState([]);

  useEffect(() => {
    async function getProducts() {
      try {
        const res = await axios.get("http://localhost:5000/api/product" + (cat ? `?category=${cat}`: ""));
        setProducts(res.data);
      } catch (err) {console.error(err)}
    }
    getProducts();
  }, [cat]);

  useEffect(() => {
     cat && setFilteredProducts(
      products.filter((item) => 
        Object.entries(filters).every(([key, value]) => item[key].includes(value.toLowerCase()))
      )
     )
  }, [products, cat, filters]);
  
  useEffect(() => {
    if (sort === "newest") {
      setFilteredProducts((prev) => 
        [...prev].sort((a, b) => a.createdAt - b.createdAt)
      )
    } else if (sort === "asc") {
      setFilteredProducts((prev) => 
        [...prev].sort((a, b) => a.price - b.price)
      )
    } else if (sort === "desc") {
      setFilteredProducts((prev) => 
        [...prev].sort((a, b) => b.price - a.price)
      )
    }
  }, [sort]);

  return (
    <Container>
      {cat ? 
        filteredProducts.map(item => (<Product item={item} key={item._id} />)) : 
        products.slice(0, 8).map(item => (<Product item={item} key={item._id} />))
      }
    </Container>
  )
}