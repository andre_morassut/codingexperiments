import { AddOutlined, RemoveOutlined } from "@material-ui/icons";
import { useEffect, useState } from "react";
import { useSelector } from 'react-redux';
import { Link, useNavigate } from "react-router-dom";
import StripeCheckout from "react-stripe-checkout";
import styled from "styled-components";
import Announcement from "../components/Announcement";
import Footer from "../components/Footer";
import Navbar from "../components/Navbar";
import { publicRequest } from "../requestMethods";
import { mobile } from "../responsive";

const KEY = process.env.REACT_APP_STRIPE;

const Container = styled.div``;
const Wrapper = styled.div`
    padding: 20px;
    ${mobile({ padding: "10px" })}
`;
const Title = styled.h1`
    font-weight: 300;
    text-align: center;
`;
const Top = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-between;
    padding: 20px;
`;
const TopButton = styled.button`
    padding: 10px;
    font-weight: 600;
    cursor: pointer;
    border: ${props => props.type === "filled" && "none"};
    background-color: ${props => props.type === "filled" ? "black" : "transparent"};
    color: ${props => props.type === "filled" && "white"};
    text-transform: uppercase;
    &:hover {
        background-color: teal;
        color: white;
    }
    &:disabled {
        cursor: not-allowed;
    }
`;
const TopTexts = styled.div`
    ${mobile({ display: "none" })}
`;
const TopText = styled.span`
    text-decoration: underline;
    cursor: pointer;
    margin: 0 10px;
`;
const Bottom = styled.div`
    display: flex;
    justify-content: space-between;
    ${mobile({ flexDirection: "column" })}
`;
const Info = styled.div`
    flex: 3;
`;
const Product = styled.div`
    display: flex;
    justify-content: space-between;
    ${mobile({ flexDirection: "column" })}
`;
const ProductDetails = styled.div`
    flex: 2;
    display: flex;
`;
const Image = styled.img`
    width: 200px;
`;
const Details = styled.div`
    padding: 20px;
    display: flex;
    flex-direction: column;
    justify-content: space-around;
`;
const ProductName = styled.span``;
const ProductId = styled.span``;
const ProductColor = styled.div`
    width: 20px;
    height: 20px;
    border-radius: 50%;
    background-color: ${props => props.color};
`;
const ProductSize = styled.span``;
const PriceDetails = styled.div`
    flex: 1;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
`;
const ProductAmountContainer = styled.div`
    display: flex;
    align-items: center;
    margin-bottom: 20px;
`;
const ProductAmount = styled.div`
    font-size: 24px;
    margin: 5px;
    ${mobile({ margin: "5px 15px" })}
`;
const ProductPrice = styled.div`
    font-size: 30px;
    font-weight: 200;
    ${mobile({ marginBottom: "20px" })}
`;
const Hr = styled.hr`
    background-color: #eee;
    border: none;
    height: 1px;
`;
const Summary = styled.div`
    flex: 1;
    border: 0.5px solid lightgray;
    border-radius: 10px;
    padding: 20px;
    height: 50vh;
`;
const SummaryTitle = styled.h1`
    font-weight: 200;
`;
const SummaryItem = styled.div`
    margin: 30px 0;
    display: flex;
    justify-content: space-between;
    font-weight: ${props => props.type === "total" && "500"};
    font-size: ${props => props.type === "total" && "24px"};
`;
const SummaryItemText = styled.span``;
const SummaryItemPrice = styled.span``;
const Button = styled.button`
    width: 100%;
    padding: 10px;
    background-color: black;
    color: white;
    font-weight: 600;
    text-transform: uppercase;
    cursor: pointer;
    &:hover {
        background-color: teal;
    }
    &:disabled {
        cursor: not-allowed;
    }
`;

export default function Cart() {
    const cart = useSelector(store => store.cart);
    const [stripeToken, setStripeToken] = useState(null);
    const navigate = useNavigate();
    
    const onToken = (t) => {
        setStripeToken(t);
    }

    const currentUser = useSelector(state => state.user && state.user.currentUser ? state.user.currentUser : {});

    useEffect(() => {
        async function makeRequest() {
            try {
                let params = {
                    tokenId: stripeToken.id,
                    amount:Math.round(cart.total.toFixed(2)*100),
                };
                let res = await publicRequest.post("/checkout/payment", params);
                navigate("/success", {
                    state: {
                        stripeData: res.data,
                        cart: cart,
                    }
                });
            } catch (error) {console.error(error)}
        }
        stripeToken && makeRequest();
    }, [stripeToken, cart, navigate, currentUser]);

  return (
    <Container>
        <Navbar />
        <Announcement />
        <Wrapper>
            <Title>Your bag</Title>
            <Top>
                <Link to="/">
                    <TopButton>Continue shopping</TopButton>
                </Link>
                <TopTexts>
                    <TopText>Shopping Bag {cart.products ? `(${cart.quantity})` : ""}</TopText>
                    <TopText>Your wishlist (0)</TopText>
                </TopTexts>
                <StripeCheckout
                        name="Recursives Shop"
                        image = "https://cdn.iconscout.com/icon/free/png-256/online-shopping-343-910325.png"
                        billingAddress
                        shippingAddress
                        description={`Your total is ${cart.total.toFixed(2)} €`}
                        amount={Math.round(cart.total.toFixed(2)*100)}
                        token={onToken}
                        stripeKey={KEY}
                    >
                    <TopButton disabled={cart.products.length === 0} type="filled">Check out now</TopButton>
                </StripeCheckout>
            </Top>
            <Bottom>
                <Info>
                    {cart.products.map(p => (<Product>
                        <ProductDetails>
                            <Image src={p.img} />
                            <Details>
                                <ProductName><strong>Product: </strong>{p.title}</ProductName>
                                <ProductId><strong>Id: </strong>{p._id}</ProductId>
                                <ProductColor color={p.color}/>
                                <ProductSize><strong>Size:</strong>{p.size}</ProductSize>
                            </Details>
                        </ProductDetails>
                        <PriceDetails>
                            <ProductAmountContainer>
                                <AddOutlined />
                                <ProductAmount>{p.quantity}</ProductAmount>
                                <RemoveOutlined />
                            </ProductAmountContainer> 
                            <ProductPrice>{`${Number.parseFloat(p.price * p.quantity).toFixed(2)} €`}</ProductPrice>
                        </PriceDetails>
                    </Product>
                   ))}
                 <Hr />
                 </Info>
                <Summary>
                    <SummaryTitle>Order summary</SummaryTitle>
                    <SummaryItem>
                        <SummaryItemText>Subtotal</SummaryItemText>
                        <SummaryItemPrice>{cart.total.toFixed(2)} €</SummaryItemPrice>
                    </SummaryItem>
                    <SummaryItem>
                        <SummaryItemText>Estimated shipping</SummaryItemText>
                        <SummaryItemPrice>4.90 €</SummaryItemPrice>
                    </SummaryItem>
                    <SummaryItem>
                        <SummaryItemText>Shipping discount</SummaryItemText>
                        <SummaryItemPrice>-4.90 €</SummaryItemPrice>
                    </SummaryItem>
                    <SummaryItem type="total">
                        <SummaryItemText>Total</SummaryItemText>
                        <SummaryItemPrice>{cart.total.toFixed(2)} €</SummaryItemPrice>
                    </SummaryItem>
                    <StripeCheckout
                        name="Recursives Shop"
                        image = "https://cdn.iconscout.com/icon/free/png-256/online-shopping-343-910325.png"
                        billingAddress
                        shippingAddress
                        description={`Your total is ${cart.total.toFixed(2)} €`}
                        amount={Math.round(cart.total.toFixed(2)*100)}
                        token={onToken}
                        stripeKey={KEY}
                    >
                        <Button disabled={cart.products.length === 0}>Checkout now</Button>
                    </StripeCheckout>
                </Summary>
            </Bottom>
        </Wrapper>
        <Footer />
    </Container>
  )
}
