import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import styled from "styled-components";
import { login } from "../redux/apiCalls";
import { mobile } from "../responsive";

const Container = styled.div`
    width: 100vw;
    height: 100vh;
    background: linear-gradient(
        rgba(255, 255, 255, 0.5),
        rgba(255, 255, 255, 0.5)
    ), 
    url("https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Mohawk_-_Millennium_Park.jpg/955px-Mohawk_-_Millennium_Park.jpg") no-repeat;
    background-size: cover;
    display: flex;
    align-items: center;
    justify-content: center;
`;
const Wrapper = styled.div`
    width: 25%;
    padding: 20px;
    background-color: white;
    ${mobile({ width: "80%" })}
`;
const Title = styled.h1`
    font-size: 24px;
    font-weight: 300;
`;
const Form = styled.form`
    display: flex;
    flex-direction: column;
`;
const Input = styled.input`
    flex: 1;
    min-width: 40%;
    margin: 10px 0;
    padding: 10px;
`;
const Button = styled.button`
    width: 40%;
    border: none;
    padding: 15px 20px;
    background-color: teal;
    color: white;
    cursor: pointer;
    margin-bottom: 10px;
    &:disabled {
        color: green;
        cursor: not-allowed
    }
`;
const Error = styled.span`
    color: red;
`;

export default function Login() {

    const [username, setUsername] = useState(null);
    const [password, setPassword] = useState(null);
    const dispatch = useDispatch();

    const {isFetching, error} = useSelector(state => state.user);

    const handleLogin = e => {
        e.preventDefault();
        login(dispatch, {
            username,
            password,
        });
    };

  return (
    <Container>
        <Wrapper>
            <Title>SIGN IN</Title>
            <Form>
                <Input placeholder="username" onChange={e => setUsername(e.target.value)} />
                <Input placeholder="password" onChange={e => setPassword(e.target.value)} type="password" />
                <Button onClick={handleLogin} disabled={isFetching}>LOGIN</Button>
                {error && <Error>Something went wrong.</Error>}
                <Link>Forgot your password?</Link>
                <Link to="/register">Create a new account</Link>
            </Form>
        </Wrapper>
    </Container>
  )
}
