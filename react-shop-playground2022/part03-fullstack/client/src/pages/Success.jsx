import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useLocation } from "react-router-dom";
import styled from "styled-components";
import Navbar from "../components/Navbar";
import { userRequestWithToken } from "../requestMethods";

const Container = styled.div``;

const SuccessContainer = styled.div`
    height: 100vh;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
`;

function Success() {
    const location = useLocation();
    const data = location.state.stripeData;
    const cart = location.state.cart;
    const currentUser = useSelector(state => state.user && state.user.currentUser ? state.user.currentUser : {_id:"UNKNOWN_USER"});
    const [newOrderId, setNewOrderId] = useState(null);

    useEffect(() => {
        async function createOrder() {
            try {
                const res = await userRequestWithToken(currentUser.accessToken).post("/order", {
                    userId: currentUser._id,
                    title: `Order - ${new Date().toLocaleString(process.env.REACT_APP_ADMIN_LOCALE)}`,
                    products:cart.products.map(v => ({
                        productId: v._id,
                        quantity: v.quantity,
                    })),
                    amount: cart.total.toFixed(2),
                    address: data.billing_details.address,
                });
                setNewOrderId(res.data._id);
            } catch (err) {console.error(err)}
        } 
        data && cart && createOrder();
    }, [data, cart, currentUser]);

    return (
        <Container>
            <Navbar />
            <SuccessContainer>
                {newOrderId ? `Success. Your order is being prepared with the number ${newOrderId}. A total of ${cart.total.toFixed(2)}€ have been billed.`: "Success, your order is being prepared. It will be sent as soon as it's ready."}
            </SuccessContainer>
        </Container>
    )
}

export default Success;