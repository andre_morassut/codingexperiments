const express = require("express");
const app = express();

const dotenv = require("dotenv");
dotenv.config();

const mongoose = require("mongoose");
mongoose.connect(process.env.MONGO_URL)
.then(() => console.log("DB Connection successful."))
.catch((err) => console.error(err));

app.use(express.json());

// TEST API
app.get("/api/test", () => {
    console.log("Test ok.");
})

// DECLARE ROUTES 
const userRoute = require("./routes/user");;
app.use("/api/user", userRoute);

const authRoute = require("./routes/auth");
app.use("/api/auth", authRoute);

const productRoute = require("./routes/product");
app.use("/api/product", productRoute);

const cartRoute = require("./routes/cart");
app.use("/api/cart", cartRoute);

const orderRoute = require("./routes/order");
app.use("/api/order", orderRoute);

app.listen(process.env.PORT ?? 4999, () => {
    console.log("Backend running on port " + process.env.PORT);
})



console.log("hello");