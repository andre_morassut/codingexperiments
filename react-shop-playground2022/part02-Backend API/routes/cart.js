const { verifyTokenAndAuth, verifyTokenAndAdmin, verifyToken } = require("./verifyToken");
const Cart = require("../models/Cart");
const router = require("express").Router();

// CREATE CART
router.post("/", verifyToken, async (req, res) => {
    const newCart = new Cart(req.body);
    try {
        const savedCart = await newCart.save();
        res.status(200).json(savedCart); 
    } catch (err) {
        res.status(500).json(err);
    }
});

// UPDATE CART
router.put("/:id", verifyTokenAndAuth, async (req, res) => {
    try {
        const updated = await Cart.findByIdAndUpdate(req.params.id, {
            $set: req.body
        }, {new: true});
        res.status(200).json(updated);
    } catch (err) {
        res.status(500).json(err);
    }
}); 

// DELETE CART
router.delete("/:id", verifyTokenAndAuth, async (req, res) => {
    try {
        await Cart.findByIdAndDelete(req.params.id);
        res.status(200).json("Cart deleted.")
    } catch (err) {
        res.status(500).json(err);
    }
})

// GET USER CART
router.get("/find/:userId", verifyTokenAndAuth, async (req, res) => {
    try {
        const o = await Cart.findOne({userId: req.params.userId});
        res.status(200).json(o); 
    } catch (err) {
        res.status(500).json(err);
    }
})

// GET ALL CARTS
router.get("/", verifyTokenAndAdmin, async (req, res) => {
    try {
        const carts = await Cart.find();
        res.status(200).json(carts);
    } catch (err) {
        res.status(500).json(err); 
    }
});

module.exports = router;