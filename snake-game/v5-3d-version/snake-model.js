export class Snake {
    constructor(x, y, size, gameBoardSize) {
        this.x = x;
        this.y = y;
        this.size = size;
        this.tail = [{x:this.x, y:this.y}];
        this.rotateX = 0;
        this.rotateY = 1; 
        this.gameBoardSize = gameBoardSize;
    }

    move() {
        var newRect;
        if (this.rotateX == 1) {
            newRect = {
                x: this.tail[this.tail.length - 1].x + this.size,
                y: this.tail[this.tail.length - 1].y
            }
        } else if (this.rotateX == -1) {
            newRect = {
                x: this.tail[this.tail.length - 1].x - this.size,
                y: this.tail[this.tail.length - 1].y
            }
        } else if (this.rotateY == 1) {
            newRect = {
                x: this.tail[this.tail.length - 1].x,
                y: this.tail[this.tail.length - 1].y - this.size
            }
        } else if (this.rotateY == -1) {
            newRect = {
                x: this.tail[this.tail.length - 1].x,
                y: this.tail[this.tail.length - 1].y + this.size
            }
        }

        this.tail.shift();
        this.tail.push(newRect);
    }
    
    checkHitWall() {
        var headTail = this.tail[this.tail.length - 1];
        if (headTail.x == -this.gameBoardSize/2 - this.size/2) {
            headTail.x = this.gameBoardSize/2 - this.size/2;
        } else if (headTail.x == this.gameBoardSize / 2 + this.size/2) {
            headTail.x = - this.gameBoardSize/2 + this.size/2;
        } else if (headTail.y == -this.gameBoardSize/2 - this.size/2) {
            headTail.y = this.gameBoardSize/2 - this.size/2;
        } else if (headTail.y == this.gameBoardSize / 2 + this.size/2) {
            headTail.y = -this.gameBoardSize / 2 + this.size/2;
        }
    }

    eatApple(apple) {
        if (!apple) return false;
        if (this.tail[this.tail.length - 1].x == apple.x && this.tail[this.tail.length - 1].y == apple.y) {
            this.tail.push({x:apple.x, y:apple.y});
            return true;
        } 
        return false;
    }

}

export class Apple {
    constructor(gameBoardSize, snake) {
        var isTouching;
        while(true) {
            isTouching = false;
            this.x = Math.floor(Math.random() * (gameBoardSize/2) / snake.size) * snake.size;
            this.y = Math.floor(Math.random() * (gameBoardSize/2) / snake.size) * snake.size;
            for (var i = 0; i < snake.tail.length; i++) {
                if (this.x == snake.tail[i].x && this.y == snake.tail[i].y) {
                    isTouching = true;
                }
            }
            this.color = "pink";
            this.size = snake.size;
            if (!isTouching) {
                break;
            }
        }
    }
}