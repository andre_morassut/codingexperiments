import * as THREE from 'three';
import { OrbitControls } from './lib/OrbitControls.js';
import { FontLoader } from './lib/FontLoader.js';
import { TextGeometry } from './lib/TextGeometry.js';
import { Snake, Apple} from './snake-model.js';


// Constants
const gameBoardSize = 130;
const cubeSize = 10;
const fov = 75;
const aspect = 2;  // the canvas default
const near = 0.1;
const far = 300;

// Game scene objects
var canvas = document.getElementById("canvas");
const CUBE_GEOMETRY = new THREE.BoxGeometry(cubeSize - 2, cubeSize - 2, cubeSize - 2);
const RENDERER = new THREE.WebGLRenderer({ canvas });
const CAMERA = new THREE.PerspectiveCamera(fov, aspect, near, far);
const SCENE = new THREE.Scene();
const MAX_FPS = 10; // The maximum FPS we want to allow
var font = null;
var materials = null;
var textMesh = null;
var last_frame_time_ms = 0; // The last time the loop was run

// game data
var appleCube;
var snakeCubes = [];
var snake = new Snake(0, 0, cubeSize, gameBoardSize);
var apple = new Apple(gameBoardSize, snake);
var score = 0;


function initialize3dScene() {
    /*



    // TODO RESPONSIVE DESIGN


        => and other todos in readme


    */

    // position the camera a little back from the origin
    CAMERA.position.set(0, -60, 90);

    // Game board
    const planeGeometry = new THREE.PlaneGeometry(gameBoardSize, gameBoardSize);
    const planeMaterial = new THREE.MeshBasicMaterial({ color: 0xaa8800, side: THREE.DoubleSide });
    const plane = new THREE.Mesh(planeGeometry, planeMaterial);
    SCENE.add(plane);

    // Light
    const color = 0xFFFFFF;
    const intensity = 1;
    const light = new THREE.DirectionalLight(color, intensity);
    light.position.set(-10, 20, 50);
    SCENE.add(light);

    const colorAmbient = 0xFFFFFF;
    const intensityAmbient = .4;
    const lightAmbient = new THREE.AmbientLight(colorAmbient, intensityAmbient);
    SCENE.add(lightAmbient);

    // ADDON Orbit controls
    const controls = new OrbitControls(CAMERA, canvas);
    controls.target.set(0, 0, 0);
    controls.update();

    // ADDON Font loader
    materials = [
        new THREE.MeshPhongMaterial( { color: 0x88ffdd, flatShading: true } ), // front
        new THREE.MeshPhongMaterial( { color: 0x88aa22 } ) // side
    ];
    const loader = new FontLoader();
    loader.load('./helvetiker_regular.typeface.json', (response) => {
        font = response;
    });
}


function createCubeInstance(geometry, color, x, y, z) {
    const material = new THREE.MeshPhongMaterial({ color: color, shininess:150 });
    const newCube = new THREE.Mesh(geometry, material);
    newCube.position.set(x, y, z);
    return newCube;
}

function removeCubeInstance(cube) {
    cube.geometry.dispose();
    cube.material.dispose();
    cube.removeFromParent();
}


// Simulate 1000 ms / 60 FPS = 16.667 ms per frame every time we run update()
// SEE https://www.cleverti.com/blog/why-a-proper-frame-rate-system-can-change-the-game/


window.onload = () => {
    initialize3dScene();
    gameLoop();
}

function gameLoop(timestamp) {
    // Throttle the frame rate.
    if (timestamp < last_frame_time_ms + (1000 / MAX_FPS)) {
        requestAnimationFrame(gameLoop);
        return;
    }
    last_frame_time_ms = timestamp;
    updateGameState();
    drawGameState();
    // Call gameLoop recursevly
    requestAnimationFrame(gameLoop);
}

function updateGameState() {
    snake.move();
    // eatApple();
    if (snake.eatApple(apple)) {
        score++;
        apple = new Apple(gameBoardSize, snake);
        if (appleCube) {
            removeCubeInstance(appleCube);
            appleCube = null;
        }
        snakeCubes.push(createCubeInstance(CUBE_GEOMETRY, 0xFFFFFF, snake.tail[snake.tail.length - 1].x, snake.tail[snake.tail.length - 1].y, 5));    
    }
    snake.checkHitWall();
}

function drawGameState() {
    if (snakeCubes.length > 0) removeCubeInstance(snakeCubes.shift());
    let cube = createCubeInstance(CUBE_GEOMETRY, 0x44aa88, snake.tail[snake.tail.length - 1].x, snake.tail[snake.tail.length - 1].y, 5);
    SCENE.add(cube);
    snakeCubes.push(cube);

    if (!appleCube) {
        appleCube = createCubeInstance(CUBE_GEOMETRY, 0xFF0000, apple.x, apple.y, 5);
        SCENE.add(appleCube);
    }
    refreshText();
    RENDERER.render(SCENE, CAMERA);
}

function refreshText() {
    if (!font) return;
    if (textMesh) {
        SCENE.remove(textMesh);
    }
    let textGeo = new TextGeometry( "Score : " + score, {
        font: font,
        size: 15,
        height: 5,
        curveSegments: 4,
        bevelThickness: 2,
        bevelSize: 1.5,
        bevelEnabled: true
    });
    textGeo.computeBoundingBox();
    const centerOffset = - 0.5 * ( textGeo.boundingBox.max.x - textGeo.boundingBox.min.x );
    textMesh = new THREE.Mesh(textGeo, materials);
    textMesh.position.x = -gameBoardSize / 2;
    textMesh.position.y = 70;
    textMesh.position.z = 10;
    textMesh.rotation.x = 1;
    SCENE.add(textMesh);
}

window.addEventListener("keydown", (event) => {
    setTimeout(() => {
        if (event.keyCode == 37 && snake.rotateX != 1) {
            snake.rotateX = -1;
            snake.rotateY = 0;
        } else if (event.keyCode == 38 && snake.rotateY != 1) {
            snake.rotateX = 0;
            snake.rotateY = -1;
        } else if (event.keyCode == 39 && snake.rotateX != -1) {
            snake.rotateX = 1;
            snake.rotateY = 0;
        } else if (event.keyCode == 40 && snake.rotateY != -1) {
            snake.rotateX = 0;
            snake.rotateY = 1;
        } 
    }, 1);
});