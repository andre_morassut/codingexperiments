import * as THREE from './lib/three.r146.module.js';
import { OrbitControls } from './lib/OrbitControls.js';

// Constants
const gameBoardSize = 130;
const cubeSize = 10;
const fov = 75;
const aspect = 2;  // the canvas default
const near = 0.1;
const far = 300;

// Shared objects
var canvas = document.getElementById("canvas");
var renderer = new THREE.WebGLRenderer({ canvas });
var camera = new THREE.PerspectiveCamera(fov, aspect, near, far);
var scene = new THREE.Scene();

class Snake {
    constructor(x, y, size) {
        this.x = x;
        this.y = y;
        this.size = size;
        this.tail = [{x:this.x, y:this.y}];
        this.rotateX = 0;
        this.rotateY = 1; 
    }

    move() {
        var newRect;
        if (this.rotateX == 1) {
            newRect = {
                x: this.tail[this.tail.length - 1].x + this.size,
                y: this.tail[this.tail.length - 1].y
            }
        } else if (this.rotateX == -1) {
            newRect = {
                x: this.tail[this.tail.length - 1].x - this.size,
                y: this.tail[this.tail.length - 1].y
            }
        } else if (this.rotateY == 1) {
            newRect = {
                x: this.tail[this.tail.length - 1].x,
                y: this.tail[this.tail.length - 1].y - this.size
            }
        } else if (this.rotateY == -1) {
            newRect = {
                x: this.tail[this.tail.length - 1].x,
                y: this.tail[this.tail.length - 1].y + this.size
            }
        }

        this.tail.shift();
        this.tail.push(newRect);
    }
}

class Apple {
    constructor() {
        var isTouching;
        while(true) {
            isTouching = false;
            this.x = Math.floor(Math.random() * (gameBoardSize/2) / snake.size) * snake.size;
            this.y = Math.floor(Math.random() * (gameBoardSize/2) / snake.size) * snake.size;
            for (var i = 0; i < snake.tail.length; i++) {
                if (this.x == snake.tail[i].x && this.y == snake.tail[i].y) {
                    isTouching = true;
                }
            }
            this.color = "pink";
            this.size = snake.size;
            if (!isTouching) {
                break;
            }
        }
    }
}

var snake = new Snake(0, 0, cubeSize);
var apple = new Apple();

let cube;
let cubes = [];

function initialize3dScene() {
    // position the camera a little back from the origin
    camera.position.set(0, 0, 100);

    // Game board
    const planeGeometry = new THREE.PlaneGeometry(gameBoardSize, gameBoardSize);
    const planeMaterial = new THREE.MeshBasicMaterial({ color: 0xffff00, side: THREE.DoubleSide });
    const plane = new THREE.Mesh(planeGeometry, planeMaterial);
    scene.add(plane);

    // Light
    const color = 0xFFFFFF;
    const intensity = 1;
    const light = new THREE.DirectionalLight(color, intensity);
    light.position.set(-10, 20, 50);
    scene.add(light);

    // ADDON Orbit controls
    const controls = new OrbitControls(camera, canvas);
    controls.target.set(0, 0, 0);
    controls.update();
}

// create the geometry for the cube
const boxWidth = cubeSize;
const boxHeight = cubeSize;
const boxDepth = cubeSize;
const geometry = new THREE.BoxGeometry(boxWidth, boxHeight, boxDepth);

function createCubeInstance(geometry, color, x, y, z) {
    const material = new THREE.MeshPhongMaterial({ color });
    const newCube = new THREE.Mesh(geometry, material);
    newCube.position.set(x, y, z);
    return newCube;
}

function removeCubeInstance(cube) {
    cube.geometry.dispose();
    cube.material.dispose();
    cube.removeFromParent();
}


// Simulate 1000 ms / 60 FPS = 16.667 ms per frame every time we run update()
// SEE https://www.cleverti.com/blog/why-a-proper-frame-rate-system-can-change-the-game/
let time_step = 1000 / 60,
delta = 0,
last_frame_time_ms = 0, // The last time the loop was run
max_FPS = 10; // The maximum FPS we want to allow


window.onload = () => {
    initialize3dScene();
    gameLoop();
}

function gameLoop(timestamp) {
    // Throttle the frame rate.
    if (timestamp < last_frame_time_ms + (1000 / max_FPS)) {
        requestAnimationFrame(gameLoop);
        return;
    }

    delta += timestamp - last_frame_time_ms;
    last_frame_time_ms = timestamp;
    updateGameState();
    drawGameState();

    // Call gameLoop recursevly
    requestAnimationFrame(gameLoop);
}

function updateGameState() {
    snake.move();
    eatApple();
    checkHitWall();
}

function checkHitWall() {
    var headTail = snake.tail[snake.tail.length - 1];
    if (headTail.x == -gameBoardSize/2 - snake.size/2) {
        headTail.x = gameBoardSize/2 - snake.size/2;
    } else if (headTail.x == gameBoardSize / 2 + snake.size/2) {
        headTail.x = - gameBoardSize/2 + snake.size/2;
    } else if (headTail.y == -gameBoardSize/2 - snake.size/2) {
        headTail.y = gameBoardSize/2 - snake.size/2;
    } else if (headTail.y == gameBoardSize / 2 + snake.size/2) {
        headTail.y = -gameBoardSize / 2 + snake.size/2;
    }
}

function eatApple() {
    if (snake.tail[snake.tail.length - 1].x == apple.x && snake.tail[snake.tail.length - 1].y == apple.y) {
        snake.tail[snake.tail.length] = {x:apple.x, y:apple.y};
        apple = new Apple();
    } 
}

function drawGameState() {
    // --- Method 1 : remove all cubes and draw them again
    for (cube of cubes) {
        removeCubeInstance(cube);
    }
    cubes = [];
    for (var i = 0; i < snake.tail.length; i++) {
        cubes.push(createCubeInstance(geometry, 0xFFFFFF, snake.tail[i].x, snake.tail[i].y, 5));
        console.log(`cube@(${snake.tail[i].x}, ${snake.tail[i].y})`);
    }
    cubes.push(createCubeInstance(geometry, 0xFF0000, apple.x, apple.y, 5));
    for (cube of cubes) {
        scene.add(cube);
    }
    renderer.render(scene, camera);
}

window.addEventListener("keydown", (event) => {
    setTimeout(() => {
        if (event.keyCode == 37 && snake.rotateX != 1) {
            snake.rotateX = -1;
            snake.rotateY = 0;
        } else if (event.keyCode == 38 && snake.rotateY != 1) {
            snake.rotateX = 0;
            snake.rotateY = -1;
        } else if (event.keyCode == 39 && snake.rotateX != -1) {
            snake.rotateX = 1;
            snake.rotateY = 0;
        } else if (event.keyCode == 40 && snake.rotateY != -1) {
            snake.rotateX = 0;
            snake.rotateY = 1;
        } 
    }, 1);
});