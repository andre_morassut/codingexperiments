import * as THREE from './lib/three.r146.module.js';
import { OrbitControls } from './lib/OrbitControls.js';
import { FontLoader } from './lib/FontLoader.js';
import { TextGeometry } from './lib/TextGeometry.js';

class Snake {
    constructor(x, y, size) {
        this.x = x;
        this.y = y;
        this.size = size;
        this.tail = [{x:this.x, y:this.y}];
        this.rotateX = 0;
        this.rotateY = 1; 
    }

    move() {
        var newRect;
        if (this.rotateX == 1) {
            newRect = {
                x: this.tail[this.tail.length - 1].x + this.size,
                y: this.tail[this.tail.length - 1].y
            }
        } else if (this.rotateX == -1) {
            newRect = {
                x: this.tail[this.tail.length - 1].x - this.size,
                y: this.tail[this.tail.length - 1].y
            }
        } else if (this.rotateY == 1) {
            newRect = {
                x: this.tail[this.tail.length - 1].x,
                y: this.tail[this.tail.length - 1].y - this.size
            }
        } else if (this.rotateY == -1) {
            newRect = {
                x: this.tail[this.tail.length - 1].x,
                y: this.tail[this.tail.length - 1].y + this.size
            }
        }

        this.tail.shift();
        this.tail.push(newRect);
    }
}

class Apple {
    constructor() {
        var isTouching;
        while(true) {
            isTouching = false;
            this.x = Math.floor(Math.random() * (gameBoardSize/2) / snake.size) * snake.size;
            this.y = Math.floor(Math.random() * (gameBoardSize/2) / snake.size) * snake.size;
            for (var i = 0; i < snake.tail.length; i++) {
                if (this.x == snake.tail[i].x && this.y == snake.tail[i].y) {
                    isTouching = true;
                }
            }
            this.color = "pink";
            this.size = snake.size;
            if (!isTouching) {
                break;
            }
        }
    }
}

// Constants
const gameBoardSize = 130;
const cubeSize = 10;
const fov = 75;
const aspect = 2;  // the canvas default
const near = 0.1;
const far = 300;

// Shared objects
var canvas = document.getElementById("canvas");
var snakeDebugPanel = document.getElementById("snake");
const appleDebugPanel = document.getElementById("apple");
var renderer = new THREE.WebGLRenderer({ canvas });
var camera = new THREE.PerspectiveCamera(fov, aspect, near, far);
var scene = new THREE.Scene();
var font = null;
var materials = null;
var textMesh = null;
var appleCube;
var snakeCubes = [];

// game data
var snake = new Snake(0, 0, cubeSize);
var apple;
var score = 0;


function initialize3dScene() {
    // position the camera a little back from the origin
    camera.position.set(0, -60, 90);

    // Game board
    const planeGeometry = new THREE.PlaneGeometry(gameBoardSize, gameBoardSize);
    const planeMaterial = new THREE.MeshBasicMaterial({ color: 0xaa8800, side: THREE.DoubleSide });
    const plane = new THREE.Mesh(planeGeometry, planeMaterial);
    scene.add(plane);

    // Light
    const color = 0xFFFFFF;
    const intensity = 1;
    const light = new THREE.DirectionalLight(color, intensity);
    light.position.set(-10, 20, 50);
    scene.add(light);

    const colorAmbient = 0xFFFFFF;
    const intensityAmbient = .4;
    const lightAmbient = new THREE.AmbientLight(colorAmbient, intensityAmbient);
    scene.add(lightAmbient);

    // ADDON Orbit controls
    const controls = new OrbitControls(camera, canvas);
    controls.target.set(0, 0, 0);
    controls.update();

    // ADDON Font loader
    materials = [
        new THREE.MeshPhongMaterial( { color: 0xffffff, flatShading: true } ), // front
        new THREE.MeshPhongMaterial( { color: 0xffffff } ) // side
    ];
    const loader = new FontLoader();
    loader.load('./helvetiker_regular.typeface.json', (response) => {
        font = response;
    });
}

// create the geometry for the cube
const boxWidth = cubeSize - 2;
const boxHeight = cubeSize - 2;
const boxDepth = cubeSize - 2;
const geometry = new THREE.BoxGeometry(boxWidth, boxHeight, boxDepth);

// const widthSegments = 12;  // ui: widthSegments
// const heightSegments = 8;  // ui: heightSegments
// const geometry = new THREE.SphereGeometry(boxWidth / 2, widthSegments, heightSegments);

function createCubeInstance(geometry, color, x, y, z) {
    const material = new THREE.MeshPhongMaterial({ color: color, shininess:150 });
    const newCube = new THREE.Mesh(geometry, material);
    newCube.position.set(x, y, z);
    return newCube;
}

function removeCubeInstance(cube) {
    cube.geometry.dispose();
    cube.material.dispose();
    cube.removeFromParent();
}


// Simulate 1000 ms / 60 FPS = 16.667 ms per frame every time we run update()
// SEE https://www.cleverti.com/blog/why-a-proper-frame-rate-system-can-change-the-game/
let time_step = 1000 / 60,
delta = 0,
last_frame_time_ms = 0, // The last time the loop was run
max_FPS = 10; // The maximum FPS we want to allow


window.onload = () => {
    initialize3dScene();
    gameLoop();
}

function gameLoop(timestamp) {
    // Throttle the frame rate.
    if (timestamp < last_frame_time_ms + (1000 / max_FPS)) {
        requestAnimationFrame(gameLoop);
        return;
    }

    delta += timestamp - last_frame_time_ms;
    last_frame_time_ms = timestamp;
    updateGameState();
    drawGameState();

    snakeDebugPanel.innerText = JSON.stringify(snake);
    appleDebugPanel.innerText = JSON.stringify(apple);

    // Call gameLoop recursevly
    requestAnimationFrame(gameLoop);
}

function updateGameState() {
    snake.move();
    eatApple();
    checkHitWall();
}

function checkHitWall() {
    var headTail = snake.tail[snake.tail.length - 1];
    if (headTail.x == -gameBoardSize/2 - snake.size/2) {
        headTail.x = gameBoardSize/2 - snake.size/2;
    } else if (headTail.x == gameBoardSize / 2 + snake.size/2) {
        headTail.x = - gameBoardSize/2 + snake.size/2;
    } else if (headTail.y == -gameBoardSize/2 - snake.size/2) {
        headTail.y = gameBoardSize/2 - snake.size/2;
    } else if (headTail.y == gameBoardSize / 2 + snake.size/2) {
        headTail.y = -gameBoardSize / 2 + snake.size/2;
    }
}

function eatApple() {
    if (!apple) return;
    if (snake.tail[snake.tail.length - 1].x == apple.x && snake.tail[snake.tail.length - 1].y == apple.y) {
        snake.tail[snake.tail.length] = {x:apple.x, y:apple.y};
        snakeCubes.push(createCubeInstance(geometry, 0xFFFFFF, snake.tail[snake.tail.length - 1].x, snake.tail[snake.tail.length - 1].y, 5));    
        apple = null;
        score++;
    } 
}

function drawGameState() {
    // --- Method 2 : only draw the new cube and remove deleted cube
    // The apple cube is managed out of the "cubes" array

    if (snakeCubes.length > 0) removeCubeInstance(snakeCubes.shift());
    let cube = createCubeInstance(geometry, 0x44aa88, snake.tail[snake.tail.length - 1].x, snake.tail[snake.tail.length - 1].y, 5);
    scene.add(cube);
    snakeCubes.push(cube);

    if (!apple) {
        if (appleCube) removeCubeInstance(appleCube);
        apple = new Apple();
        appleCube = createCubeInstance(geometry, 0xFF0000, apple.x, apple.y, 5);
        scene.add(appleCube);
    }

    refreshText();

    renderer.render(scene, camera);
}

function refreshText() {
    if (!font) return;
    if (textMesh) {
        scene.remove(textMesh);
    }
    let textGeo = new TextGeometry( "Score : " + score, {
        font: font,
        size: 15,
        height: 5,
        curveSegments: 4,
        bevelThickness: 2,
        bevelSize: 1.5,
        bevelEnabled: true
    });
    textGeo.computeBoundingBox();
    const centerOffset = - 0.5 * ( textGeo.boundingBox.max.x - textGeo.boundingBox.min.x );
    textMesh = new THREE.Mesh(textGeo, materials);
    textMesh.position.x = -gameBoardSize / 2;
    textMesh.position.y = 70;
    textMesh.position.z = 0;
    textMesh.rotation.x = Math.PI/4;
    scene.add(textMesh);
}

window.addEventListener("keydown", (event) => {
    setTimeout(() => {
        if (event.keyCode == 37 && snake.rotateX != 1) {
            snake.rotateX = -1;
            snake.rotateY = 0;
        } else if (event.keyCode == 38 && snake.rotateY != 1) {
            snake.rotateX = 0;
            snake.rotateY = -1;
        } else if (event.keyCode == 39 && snake.rotateX != -1) {
            snake.rotateX = 1;
            snake.rotateY = 0;
        } else if (event.keyCode == 40 && snake.rotateY != -1) {
            snake.rotateX = 0;
            snake.rotateY = 1;
        } 
    }, 1);
});