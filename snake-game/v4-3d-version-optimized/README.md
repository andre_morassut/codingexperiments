## SNAKE 3D - optimized

Adapted from 2d version.

This method of updating the scene was previously : remove all the cubes and draw them again from the "tail" data.
In this version, we will draw only the new cube and remove the old one.

Also, I added a 3D text to display the score.